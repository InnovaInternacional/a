# Contributing to Fiori App

The following is a set of guidelines for contributing with developing new components.

## Getting started

### Environment

To develop new packages, you only need to install Node with NPM. Find below the default versions this repo was built with:

- Node: `v14+`
- NPM: `v6`

### Main commands

This repo is managed with [sigc monorepo](https://gitlab.com/InnovaInternacional/siprocura.git) tool.

Some base commands you should acknowledge :

- `yarn dev`.
- `yarn build`
- `yarn lint`

## Naming Conventions

```js
/** @private */
this._identifier = 100
/** @public */
/**
 * @param {string}  p1 - A string param.
 * @param {string} p2 - An optional param (Closure syntax)
 * @param {string} [p3] - Another optional param (JSDoc syntax).
 * @param {string} [p4="test"] - An optional param with a default value
 * @return {string} This is the result
 */
function onClickHandler(p1, p2, p3, p4) {
  // TODO
}
/** @private */
/**
 * @param {string}  p1 - A string param.
 * @param {string} p2 - An optional param (Closure syntax)
 * @param {string} [p3] - Another optional param (JSDoc syntax).
 * @param {string} [p4="test"] - An optional param with a default value
 * @return {string} This is the result
 */
function _clickHandler(p1, p2, p3, p4) {
  // TODO
}
```

### Comments

- [ts-check-js](https://www.typescriptlang.org/docs/handbook/intro-to-js-ts.html)
- [types](https://www.typescriptlang.org/docs/handbook/jsdoc-supported-types.html#param-and-returns)
- [@openui5/ts-types](https://www.npmjs.com/package/@openui5/ts-types?activeTab=readme)
- [How to improve Fiori/UI5 code quality with Typescript, not using Typescript](https://blogs.sap.com/2019/10/14/how-to-improve-fioriui5-code-quality-with-typescript-not-using-typescript/)
- [ui5-typescript-helloworld](https://github.com/SAP-samples/ui5-typescript-helloworld/blob/main/step-by-step.md)

### Event Handlers

Many presentational components give feedback from user interaction calling provided callbacks.

#### `on{Event}`

```xml
<Button press="{onClickHandler}" />
<TextArea change="{onChangeHandler}" />
```

#### `on{SubContext}{Event}`

For compound component, we may need to provide several callbacks.

```xml
<Dialog>
<beginButton>
  <Button id="transferButton"
    text="{i18n>Home.0001}"
    tooltip="{i18n>Home.0001}"
    press=".onAssingUserHandler">
  </Button>
</beginButton>
</Dialog>
```
