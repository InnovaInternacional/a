/*!
 * ${copyright}
 */
sap.ui.define(
  [
    './errorHandlers',
    'sap/ui/core/IconPool',
    'sap/ui/core/routing/History',
    'sap/ui/core/UIComponent',
    'sap/ui/Device',
    'com/innova/sige/model/models',
  ],
  /**
   * Module dependencies
   * @namespace com.innova.sigc
   * @param {typeof sap.ui.core.UIComponent} UIComponent
   * @param {typeof sap.ui.core.routing.History} History
   *
   */
  (errorHandlers, IconPool, History, UIComponent, Device, models) =>
    UIComponent.extend('com.innova.sige.Component', {
      metadata: {
        manifest: 'json',
      },
      _sContentDensityClass: undefined,
      _sTimeoutId: undefined,
      // TextPool de la aplicación
      _oTextPool: {},
      /**
       * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
       * @public
       * @override
       */
      init(...args) {
        // call the base component's init function
        UIComponent.prototype.init.apply(this, args)

        this._bindFonts()

        // Set aplications model
        const oMainModel = models.createMainModel()
        this.setModel(oMainModel, 'main')
        // eslint-disable-next-line no-console
        console.log('main', oMainModel)

        // Set store model
        this.setModel(models.createStoreModel(), 'store')

        // create the views based on the url/hash
        this.getRouter().initialize()
        this.hideLoader(300)
      },
      /**
       * The component is destroyed by UI5 automatically.
       * @public
       * @override
       */
      destroy(...args) {
        // call the base component's destroy function
        UIComponent.prototype.destroy.apply(this, args)
      },
      /**
       * This method can be called to determine whether the sapUiSizeCompact or sapUiSizeCozy
       * design mode class should be set, which influences the size appearance of some controls.
       * @public
       * @return {string} css class, either 'sapUiSizeCompact' or 'sapUiSizeCozy' - or an empty string if no css class should be set
       *
       * @version ${version}
       */
      getContentDensityClass() {
        if (this._sContentDensityClass === undefined) {
          // check whether FLP has already set the content density class; do nothing in this case
          if (
            document.body.classList.contains('sapUiSizeCozy') ||
            document.body.classList.contains('sapUiSizeCompact')
          ) {
            this._sContentDensityClass = ''
          } else if (!Device.support.touch) {
            // apply "compact" mode if touch is not supported
            this._sContentDensityClass = 'sapUiSizeCompact'
          } else {
            // "cozy" in case of touch support; default for most sap.m controls, but needed for desktop-first controls like sap.ui.table.Table
            this._sContentDensityClass = 'sapUiSizeCozy'
          }
        }
        return this._sContentDensityClass
      },
      /**
       * @function
       * @name errorHandler
       * @description - Se encarga de recibir el error de un request.
       *
       * @public
       * @param {Object} e - Data que se recibe del servicio.
       * @param {sap.ui.core.mvc.Controller}  controller - Controllador que llama la función
       * @returns {void} - Noting to return.
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      errorHandler(error) {
        errorHandlers(error)
      },
      /**
       * @function
       * @name getMessageTextPool
       * @description - Retorna un mensaje del modelos de los títulos globales de la aplicacion.
       *
       * @public
       * @param {string} sKey String con el nombre de la propiedad a buscar.
       * @returns {string} con el texto encontrado, o error sino se encuentra.
       *
       * @author Edwin Valencia <evalecia@innovainternacional.biz>
       * @version ${version}
       */
      getMessageTextPool(sKey) {
        const oTextPool = this._oTextPool
        return Object.prototype.hasOwnProperty.call(oTextPool, sKey)
          ? oTextPool[sKey]
          : ''
      },
      /**
       * @function
       * @name hideLoader
       * @description - Se encarga de ocultar el loader del index.html.
       *
       * @public
       * @param {int} iDelay - Delay para ocultar el loader.
       * @returns {void} - Noting to return.
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      hideLoader(iDelay) {
        if (iDelay && iDelay > 0) {
          if (this._sTimeoutId) {
            clearTimeout(this._sTimeoutId)
            this._sTimeoutId = null
          }
          this._sTimeoutId = setTimeout(() => {
            document.getElementById('loader').style.display = 'none'
          }, iDelay)
        }
      },
      /**
       * @function
       * @name showLoader
       * @description - Se encarga de mostrar el loader del index.html.
       *
       * @public
       * @returns {void} - Noting to return.
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      showLoader() {
        document.getElementById('loader').style.display = 'block'
      },
      /**
       * @function
       * @name onNavBack()
       * @description - Event handler for navigating back.
       *  It there is a history entry we go one step back in the browser history
       *  If not, it will replace the current entry of the browser history with the master route.
       * @public
       * @returns {void} - No retorna nada
       *
       * @author Edwin Valencia <evalecia@innovainternacional.biz>
       * @version ${version}
       */
      onNavBack() {
        const sPreviousHash = History.getInstance().getPreviousHash()

        if (sPreviousHash !== undefined) {
          window.history.go(-1)
        } else {
          this.getRouter().navTo('home', {}, {}, true)
        }
      },

      /**
       * @function
       * @name _bindFonts
       * @description - Enlaza los iconos de la innova con el Pool de Iconos de SAP.
       *
       * @private
       * @returns {void} - Noting to return.
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      _bindFonts() {
        const oTNTConfig = {
          fontFamily: 'SAP-icons-TNT',
          fontURI: sap.ui.require.toUrl('sap/tnt/themes/base/fonts/'),
        }

        // register TNT icon font
        IconPool.registerFont(oTNTConfig)
      },
    })
)
