/*!
 * ${copyright}
 */
sap.ui.define(
  [],
  () =>
    /**
     * @function
     * @name isEmpty
     * @description - Valida si no es un valor null o undefined
     *
     * @public
     * @param {any} value - Mensaje a mostrar
     * @returns {boolean}
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */

    (value) =>
      // null or undefined
      value == null ||
      // has length and it's zero
      (Object.prototype.hasOwnProperty.call(value, 'length') &&
        value.length === 0) ||
      // is an Object and has no keys
      (value.constructor === Object && Object.keys(value).length === 0)
)
