sap.ui.define([], () =>
  /**
   * @function
   * @name addPatternToText
   * @description - Retorna el texto más el patrón (*) al principio y/o final
   *  si el tamaño del mismo lo permite.
   *
   * @public
   * @param {string} text Texto a cocatenar con *.
   * @param {int} maxLength Maximo tamaño del texto que puede tener.
   * @returns {string} texto concatenado.
   *
   * @author Edwin Valencia <evalencia@innovainternacional.biz>
   * @version ${version}
   */
  (text, maxLength) => {
    if (text.length < maxLength && text.length + 2 <= maxLength) {
      return ['*', text, '*'].join('')
    }
    if (text.length < maxLength && text.length + 1 === maxLength) {
      return [text, '*'].join('')
    }
    return text
  }
)
