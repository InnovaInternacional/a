/*!
 * ${copyright}
 */
sap.ui.define(
  ['./isEmpty'],
  (isEmpty) =>
    /**
     * @function
     * @name hasAllPropsEmpty
     * @description - Valida si tiene todas sus propiedades vacías
     *
     * @private
     * @param {object} obj - Objeto a validar
     * @returns {boolean}
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    (obj = {}) =>
      Object.values(obj).every(isEmpty)
)
