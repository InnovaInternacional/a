/*!
 * ${copyright}
 */
sap.ui.define(
  [],
  () =>
    /**
     * @function
     * @name checkIfNumberBetween
     * @description - Checks if a number is between two other numbers
     *
     * @public
     * @param {string} value - value to be checked
     * @param {number} min - minimum value
     * @param {number} max - maximum value
     * @returns {boolean} - Returns true if the number is between the two other numbers
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    (value, min, max) =>
      parseFloat(value) >= min && parseFloat(value) <= max
)
