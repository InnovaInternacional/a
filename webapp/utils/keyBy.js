/*!
 * ${copyright}
 */
sap.ui.define(
  [],
  () =>
    /**
     * @function
     * @name keyBy
     * @description - Convierte un arreglo en un objeto de indices, según la propiedad pasada
     *
     * @private
     * @param {object} arr - Arreglo a convertir
     * @param {string} key - Propiedad a usar
     * @returns {object}
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    (arr, key) =>
      arr.reduce((acc, el) => {
        acc[`${el[`${key}`]}`] = el
        return acc
      }, {})
)
