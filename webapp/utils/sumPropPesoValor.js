/*!
 * ${copyright}
 */
sap.ui.define(
  [],
  () =>
    /**
     * @function
     * @name sumPropPesoValor
     * @description - Sum property property pesoValor
     *
     * @public
     * @param {number} acc - Accumulator
     * @param {object} el - Element
     * @returns {number}
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    (acc, el) =>
      acc + el.pesoValor
)
