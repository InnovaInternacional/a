sap.ui.define([], () =>
  /**
   * @function
   * @name redirect
   * @description - Se encarga redirigir la página a una URL específica.
   *
   * @public
   * @param {Object} url - URL a redirigir.
   * @returns {void} - No retorna nada.
   *
   * @author Edwin Valencia <evalencia@innovainternacional.biz>
   * @version ${version}
   */
  (url) => {
    const ua = navigator.userAgent.toLowerCase()
    const isIE = ua.indexOf('msie') !== -1
    const version = parseInt(ua.substr(4, 2), 10)

    // Internet Explorer 8 and lower
    if (isIE && version < 9) {
      const link = document.createElement('a')
      link.href = url
      document.body.appendChild(link)
      link.click()
    } else {
      // All other browsers can use the standard window.location.href (they don't lose HTTP_REFERER like Internet Explorer 8 & lower does)
      window.location.replace(url)
    }
  }
)
