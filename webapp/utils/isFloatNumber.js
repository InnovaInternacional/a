/*!
 * ${copyright}
 */
sap.ui.define(
  [],
  () =>
    /**
     * @function
     * @name isFloatNumber
     * @description - Valida si el valor del Input es un numero flotante
     *
     * @public
     * @param {string} value - valor
     * @returns {boolean} - si es valido
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    (value) =>
      !(
        /[a-zA-Z ]+/.test(value) ||
        /[!@#$%`·º´^&ª*¨Ç()_+\-=[\]{};':"\\|<>/¿?¡!]/.test(value)
      )
)
