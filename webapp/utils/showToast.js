/*!
 * ${copyright}
 */
sap.ui.define(['sap/m/MessageToast'], (MessageToast) =>
  /**
   * @function
   * @name showToast
   * @description - Permite mostrar un sap.m.MessageToast con duración de 3s.
   *
   * @public
   * @param {string} sMessage - Mensaje para mostrar.
   * @returns {void} - No retorna nada
   *
   * @author Edwin Valencia <evalencia@innovainternacional.biz>
   * @version ${version}
   */
  (sMessage) => {
    MessageToast.show(sMessage, {
      duration: 3000,
      width: 'auto',
      closeOnBrowserNavigation: false,
    })
  }
)
