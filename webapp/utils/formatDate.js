/*!
 * ${copyright}
 */
sap.ui.define(['sap/ui/core/format/DateFormat'], (DateFormat) => {
  const oDateFormat = DateFormat.getDateInstance({ pattern: 'yyyy-MM-dd' })

  /**
   * @function
   * @name formatDate
   * @description - Retorna una fecha formateada al formato que SAP recibe.
   *
   * @public
   * @param {Date} dDate - Fecha a formatear.
   * @returns {string} - Fecha formateada.
   *
   * @author Edwin Valencia <evalencia@innovainternacional.biz>
   * @version ${version}
   */
  return (dDate) => oDateFormat.format(dDate)
})
