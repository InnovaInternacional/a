/*!
 * ${copyright}
 */
sap.ui.define(
  ['com/innova/vendor/file-saver'],
  (fileSaver) =>
    /**
     * @function
     * @name downloadFile
     * @description - Build a URL by appending params to the end
     *
     * @private
     * @param {object} params - The params to be appended
     * @returns {promise} The formatted url
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version} - se crea la función.
     */
    ({ blob, filename = '' }) =>
      fileSaver.saveAs(blob, filename)
)
