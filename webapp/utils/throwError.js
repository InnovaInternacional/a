/*!
 * ${copyright}
 */
sap.ui.define([], () =>
  /**
   * @function
   * @name throwError
   * @description - Lanzar error si la condición es verdadera
   *
   * @public
   * @param {boolean} condition - Condición
   * @param {string} message - Mensaje del error
   * @returns {void} - No retorna nada
   *
   * @author Edwin Valencia <evalencia@innovainternacional.biz>
   * @version ${version}
   */
  (condition, message) => {
    if (condition) {
      throw new Error(message)
    }
  }
)
