sap.ui.define(
  ['../model/constant', 'com/innova/vendor/axios'],
  (constant, axios) => {
    const path = `https://sige-api.innovapps.net/api/`

    const { CancelToken } = axios
    const sources = []
    return {
      get(route) {
        const source = CancelToken.source()
        sources.push(source)
        return axios.get(path + route, {
          headers: {
            'Content-Type': 'application/json; charset=utf-8',
            Authorization: window.sessionStorage.getItem(
              constant.DEFAULT_SESSION.token
            ),
          },
          cancelToken: source.token,
        })
      },
      find(route, query, data) {
        const source = CancelToken.source()
        sources.push(source)
        return axios.post(`${path + route}/${query}`, data, {
          headers: {
            'Content-Type': 'application/json; charset=utf-8',
          },
          cancelToken: source.token,
        })
      },
      post(route, data) {
        const source = CancelToken.source()
        sources.push(source)
        return axios.post(path + route, data, {
          headers: {
            'Content-Type': 'application/json; charset=utf-8',
            Authorization: window.sessionStorage.getItem(
              constant.DEFAULT_SESSION.token
            ),
          },
          cancelToken: source.token,
        })
      },
      register(route, token, data) {
        const source = CancelToken.source()
        sources.push(source)
        return axios.post(path + route, data, {
          headers: {
            'Content-Type': 'application/json; charset=utf-8',
            Authorization: `Bearer ${token}`,
          },
          cancelToken: source.token,
        })
      },
      update(route, data) {
        const source = CancelToken.source()
        sources.push(source)
        return axios.put(path + route, data, {
          headers: {
            'Content-Type': 'application/json; charset=utf-8',
          },
          cancelToken: source.token,
        })
      },
      delete(route, data) {
        const source = CancelToken.source()
        sources.push(source)
        return axios.delete(path + route, {
          headers: {
            'Content-Type': 'application/json; charset=utf-8',
          },
          data,
          cancelToken: source.token,
        })
      },
      downloadAttachment({ url }) {
        const source = CancelToken.source()
        sources.push(source)
        return axios({
          method: 'get',
          url: `${path}${url}`,
          responseType: 'blob',
          headers: {
            'Content-Type': 'application/json; charset=utf-8',
          },
          cancelToken: source.token,
        })
      },
      /* 'Accept': 'application/msword' */
      spread(cb) {
        return axios.spread(cb)
      },
      reset(route, data) {
        const source = CancelToken.source()
        sources.push(source)
        return axios.post(path + route, data, {
          headers: {
            'Content-Type': 'application/json; charset=utf-8',
            Authorization: `Bearer ${data.token}`,
          },
          cancelToken: source.token,
        })
      },
      login(route, data) {
        const source = CancelToken.source()
        sources.push(source)
        return axios.post(path + route, data, {
          headers: {
            'Content-Type': 'application/json; charset=utf-8',
          },
          cancelToken: source.token,
        })
      },
      abort(message) {
        return new Promise((resolve, reject) => {
          try {
            sources.forEach((s) => {
              s.cancel(message || 'Operation canceled by the user.')
            })
            sources.length = 0
            resolve()
          } catch (error) {
            reject()
          }
        })
      },
      isCancel(error) {
        return axios.isCancel(error)
      },
    }
  }
)
