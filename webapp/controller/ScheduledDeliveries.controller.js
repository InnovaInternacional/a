/* eslint-disable prefer-destructuring */
/* eslint-disable no-param-reassign */
sap.ui.define(
  [
    './BaseController',
    'sap/m/Dialog',
    'sap/m/DialogType',
    'sap/m/Button',
    'sap/m/ButtonType',
    'sap/m/Text',
    'com/innova/sige/service/http',
    'com/innova/sige/model/constant',
    'sap/ui/model/json/JSONModel',

    'sap/m/MessageBox',
    'sap/m/MessageToast',
    'sap/ui/core/Fragment',
    'sap/ui/core/util/Export',
    'sap/ui/core/util/ExportTypeCSV',
    // 'sap/ui/export/Spreadsheet',
  ],
  (
    BaseController,
    Dialog,
    DialogType,
    Button,
    ButtonType,
    Text,
    http,
    constant,
    JSONModel,
    MessageBox,
    MessageToast,
    Fragment,
    Export,
    ExportTypeCSV
    // Spreadsheet
  ) =>
    BaseController.extend('com.innova.sige.controller.ScheduledDeliveries', {
      onInit() {
        this._oRouter = this.getRouter()
        const oRouter = this.getOwnerComponent().getRouter()
        const oDeliveryModel = new JSONModel()
        this.getView().setModel(oDeliveryModel, 'ScheduledDeliveriesModel')

        oRouter
          .getRoute('ScheduledDeliveries')
          .attachPatternMatched(this._onObjectMatched, this)
        //   this.resetDetailsData()
      },
      _onObjectMatched() {
        this.getView()
          .byId('sDeliveryTableId')
          .setText(
            `${this.getResourceBundle().getText(
              'ScheduledDeliveries.0010'
            )} (0)`
          )

        this.getScheduledDeliveries()
      },
      getScheduledDeliveries() {
        const oVendorData = this.getOwnerComponent()
          .getModel('VendorsModel')
          .getProperty('/searchData')
        if (oVendorData) {
          // let sDescriptions = '';
          // if(oVendorData.shortText){

          //   for(let i=0;i<oVendorData.shortText.length - 1;i += 1){
          //     sDescriptions += oVendorData.shortText[i] + ' ';
          //   }
          //   sDescriptions += oVendorData.shortText[oVendorData.shortText.length - 1] ;
          // }
          const oPayload = {
            lifnr: oVendorData.lifnr,
            id: oVendorData.id,
            status: oVendorData.status === '' ? [] : [oVendorData.status],
            ebeln: [],
            matnr: oVendorData.materialNumber,
            werks: oVendorData.plant,
            lgort: oVendorData.storageLoc,
            delivery_date_to: oVendorData.toDelDate,
            delivery_date_from: oVendorData.fromDelDate,
          }
          const that = this

          // http
          //   .get(
          //     `${constant.api.PURCHASE}?lifnr=${lifnr}&ebeln=${purchaseDocNumber}&txz01=${shortText}&matnr=${materialNumber}&werks=${plant}&lgort=${storageLoc}&eindtTo=${toDelDate}&eindtFrom=${fromDelDate}`
          //   )
          //   .then(this._handlePurchaseOrderData.bind(this))

          http
            .post(`${constant.api.DELIVERY_SEARCH}`, oPayload)
            .then((data) => {
              that.handleScheduledDeliveries(data.data)
            })
            .catch((error) => {
              MessageBox.error(error.message)
            })
        }
      },
      handleScheduledDeliveries(data) {
        data.forEach((oItem) => {
          oItem.fromtime = oItem.deliveryTimeFrom.split('T')[1].split('.')[0]
          oItem.totime = oItem.deliveryTimeTo.split('T')[1].split('.')[0]
          oItem.isSplitQty = oItem.deliveryItems
            .map((oValue) => oValue.ebelp.ebeln + oValue.ebelp.ebelp)
            .some(
              (oValue2, index, array) =>
                array.indexOf(oValue2) !== array.lastIndexOf(oValue2)
            )
          if (oItem.status === '1') {
            oItem.status_icon = 'sap-icon://appointment'
            oItem.tooltip = this.getResourceBundle().getText('Status.0001')
          } else if (oItem.status === '2') {
            oItem.status_icon = 'sap-icon://redo'
            oItem.tooltip = this.getResourceBundle().getText('Status.0002')
            oItem.status_iconcolor = 'Negative'
          } else if (oItem.status === '9') {
            oItem.status_icon = 'sap-icon://sys-cancel-2'
            oItem.tooltip = this.getResourceBundle().getText('Status.0003')
            oItem.status_iconcolor = 'Negative'
          } else if (oItem.status === '4') {
            oItem.status_icon = 'sap-icon://complete'
            oItem.tooltip = this.getResourceBundle().getText('Status.0004')
            oItem.status_iconcolor = 'Positive'
          } else if (oItem.status === '3') {
            oItem.status_icon = 'sap-icon://accept'
            oItem.tooltip = this.getResourceBundle().getText('Status.0005')
            oItem.status_iconcolor = 'Default'
          }
        })
        this.getView()
          .getModel('ScheduledDeliveriesModel')
          .setProperty('/scheduleddeliveries', data)
        this.getView().getModel('ScheduledDeliveriesModel').refresh(true)
        this.getView()
          .byId('sDeliveryTableId')
          .setText(
            `${this.getResourceBundle().getText('ScheduledDeliveries.0010')} (${
              data.length
            })`
          )
      },
      onNavToMasterPage() {
        this._oRouter.navTo('SearchOrders')
      },
      onSaveDeliveryPress() {
        this.handleDeliveryUpdate()
      },
      onCancelDeliveryPress() {
        const _self = this
        MessageBox.confirm(
          this.getResourceBundle().getText('ScheduledDeliveries.0017'),
          {
            actions: [
              this.getResourceBundle().getText('Common.0005'),
              MessageBox.Action.CANCEL,
            ],
            title: this.getResourceBundle().getText('ScheduledDeliveries.0012'),
            emphasizedAction: this.getResourceBundle().getText('Common.0005'),
            onClose: (sAction) => {
              if (sAction === this.getResourceBundle().getText('Common.0005')) {
                // MessageToast.show(
                //   this.getResourceBundle().getText('Common.0005')
                // )
                _self.handleDeliveryCancel()
              }
            },
          }
        )
      },
      onCalendarHistoryPress() {
        const oView = this.getView()
        this.getDeliveryHistory()
        if (!this._oCustomerHistoryDialog) {
          this._oCustomerHistoryDialog = Fragment.load({
            id: oView.getId(),
            name: 'com.innova.sige.view.Fragments.CustomerHistory',
            controller: this,
          }).then((oDialog) => {
            oDialog.setModel(oView.getModel('DeliveryModel'), 'DeliveryModel')
            oView.addDependent(oDialog)
            return oDialog
          })
        }

        this._oCustomerHistoryDialog.then((oDialog) => {
          oDialog.open()
        })
      },
      onHistoryContinuePress() {
        this._oCustomerHistoryDialog.then((oDialog) => {
          oDialog.close()
        })
        this.getView().byId('sSchdeuledDeliveriesTableId').removeSelections()
      },
      onDeliverySchedulePress() {
        this._oRouter.navTo('CalendarView', {
          source: '2',
        })
      },
      onDeliveryIdPress(oEvent) {
        const oSelectedDelivery = oEvent
          .getSource()
          .getBindingContext('ScheduledDeliveriesModel')
          .getObject()
        this._oRouter.navTo('ScheduledDeliveryDetail', {
          deliveryid: oSelectedDelivery.id,
        })
      },
      onReschedulePress(oEvent) {
        this.oSelectedObject = oEvent
          .getSource()
          .getBindingContext('ScheduledDeliveriesModel')
          .getObject()
        const oControl = oEvent.getSource()
        const oView = this.getView()

        // create popover
        if (
          oEvent
            .getSource()
            .getBindingContext('ScheduledDeliveriesModel')
            .getObject().status === '2'
        ) {
          const oSelectedObject = oEvent
            .getSource()
            .getBindingContext('ScheduledDeliveriesModel')
            .getObject()
          this.getDeliveryProposals(oSelectedObject.id)
          if (!this._pPopover) {
            this._pPopover = Fragment.load({
              id: oView.getId(),
              name: 'com.innova.sige.view.Fragments.RescheduleDelivery',
              controller: this,
            }).then((oPopover) => {
              oView.addDependent(oPopover)
              return oPopover
            })
          }
          this._pPopover.then((oPopover) => {
            oPopover.openBy(oControl)
          })
        }
      },
      getDeliveryProposals(sId) {
        const oPayload = {
          lifnr: '100045',
          id: sId ? [sId] : null,
          status: [],
          ebeln: [],
          matnr: [],
          werks: [],
          lgort: [],
          delivery_date_to: null,
          delivery_date_from: null,
        }
        const that = this
        http
          .post(`${constant.api.DELIVERY_SEARCH}`, oPayload)
          .then((data) => {
            data.data[0].deliveryProposals.forEach((oItem) => {
              oItem.fromtime = oItem.deliveryTimeFrom
                .split('T')[1]
                .split('.')[0]
              oItem.totime = oItem.deliveryTimeTo.split('T')[1].split('.')[0]
            })
            that
              .getView()
              .getModel('ScheduledDeliveriesModel')
              .setProperty('/proposals', data.data[0])
          })
          .catch((error) => {
            MessageBox.error(error.message)
          })
      },
      onProposeNewDeliveryPress() {
        this._oRouter.navTo('ProposeDelivery', {
          deliveryid: this.oSelectedObject.id,
        })
      },
      createColumnConfig() {
        const aCols = []

        aCols.push({
          name: this.getResourceBundle().getText('ScheduledDeliveries.0001'),
          template: {
            content: '{id}',
          },
          // property: 'id',
          // type: 'String',
        })

        aCols.push({
          name: this.getResourceBundle().getText('ScheduledDeliveries.0002'),
          // type: 'String',
          template: {
            content: '{status}',
          },
          // property: 'status',
        })

        aCols.push({
          name: this.getResourceBundle().getText('ScheduledDeliveries.0003'),
          // type: 'String',
          template: {
            content: '{deliveryDate}',
          },
          // property: 'deliveryDate',
        })

        aCols.push({
          name: this.getResourceBundle().getText('ScheduledDeliveries.0004'),
          // type: 'String',
          template: {
            content: '{fromtime}',
          },
          // property: 'fromtime',
        })

        aCols.push({
          name: this.getResourceBundle().getText('ScheduledDeliveries.0005'),
          // type: 'String',
          // property: 'totime',
          template: {
            content: '{totime}',
          },
        })

        aCols.push({
          name: this.getResourceBundle().getText('ScheduledDeliveries.0006'),
          // type: 'String',
          // property: 'carrier',
          template: {
            content: '{carrier}',
          },
        })

        aCols.push({
          name: this.getResourceBundle().getText('ScheduledDeliveries.0007'),
          // type: 'String',
          // property: 'driver',
          template: {
            content: '{driver}',
          },
        })

        aCols.push({
          name: this.getResourceBundle().getText('ScheduledDeliveries.0008'),
          // type: 'String',
          // property: 'carrierPhone',
          template: {
            content: '{carrierPhone}',
          },
        })
        aCols.push({
          name: this.getResourceBundle().getText('ScheduledDeliveries.0009'),
          // type: 'String',
          // property: 'carrierEmail',
          template: {
            content: '{carrierEmail}',
          },
        })

        return aCols
      },

      onExportPress() {
        // let aCols = ''
        // let oRowBinding = ''
        // // let oSettings = {}
        // // let oSheet = {}
        // if (!this._oTable) {
        //   this._oTable = this.byId('sSchdeuledDeliveriesTableId')
        // }
        // oRowBinding = this._oTable.getBinding('items')
        // aCols = this.createColumnConfig()
        // oSettings = {
        //   workbook: {
        //     columns: aCols,
        //     hierarchyLevel: 'Level',
        //   },
        //   dataSource: oRowBinding,
        //   fileName: 'ScheduledDeliveries.xlsx',
        // }
        // oSheet = new Spreadsheet(oSettings)
        // oSheet.build().finally(() => {
        //   oSheet.destroy()
        // })
        const oExport = new Export({
          // Type that will be used to generate the content. Own ExportType's can be created to support other formats
          exportType: new ExportTypeCSV({
            separatorChar: ';',
          }),

          // Pass in the model created above
          models: this.getView().getModel('ScheduledDeliveriesModel'),

          // binding information for the rows aggregation
          rows: {
            path: '/scheduleddeliveries',
          },

          // column definitions with column name and binding info for the content

          columns: this.createColumnConfig(),
        })

        // download exported file
        oExport
          .saveFile()
          .catch(() => {})
          .then(() => {
            oExport.destroy()
          })
      },
      handleCreatePayload(sStatus) {
        const oSelectedDeliveries = this.getView()
          .byId('sSchdeuledDeliveriesTableId')
          .getSelectedItems()
        const aDeliveries = []
        oSelectedDeliveries.forEach((oItem) => {
          const aProposals = []
          const oSelectedObject = oItem
            .getBindingContext('ScheduledDeliveriesModel')
            .getObject()
          oSelectedObject.deliveryProposals.forEach((oItem2) => {
            const oProposal = {
              deliveryDate: oItem2.deliveryDate,
              deliveryTimeFrom: oItem2.deliveryTimeFrom,
              deliveryTimeTo: oItem2.deliveryTimeTo,
              notes: oItem2.notes,
            }
            aProposals.push(oProposal)
          })
          const oPayload = {
            id: `${oSelectedObject.id}`,
            status: sStatus,
            proposals: aProposals,
          }
          aDeliveries.push(oPayload)
        })
        return aDeliveries
      },
      handleDeliveryCancel() {
        const _self = this
        const aDeliveries = this.handleCreatePayload('9')
        http
          .post(`${constant.api.DELIVERY_STATUS}`, aDeliveries)
          .then(() => {
            MessageToast.show(
              this.getResourceBundle().getText('ScheduledDeliveries.0019')
            )
            _self
              .getView()
              .byId('sSchdeuledDeliveriesTableId')
              .removeSelections()
            _self.getScheduledDeliveries()
          })
          .catch((error) => {
            MessageBox.error(error.message)
          })
      },
      handleDeliveryUpdate() {
        const _self = this
        const aSelectedDeliveries = this.getView()
          .byId('sSchdeuledDeliveriesTableId')
          .getSelectedItems()
        const aPayload = []

        aSelectedDeliveries.forEach((oItem) => {
          const oSelectedDelivery = oItem
            .getBindingContext('ScheduledDeliveriesModel')
            .getObject()
          const oPayload = {
            id: `${oSelectedDelivery.id}`,
            driver: oSelectedDelivery.driver,
            carrier: oSelectedDelivery.carrier,
            carrierEmail: oSelectedDelivery.carrierEmail,
            carrierPhone: oSelectedDelivery.carrierPhone,
            carrierPlates: oSelectedDelivery.carrierPlates,
            eqptId: oSelectedDelivery.eqptId.id,
          }
          aPayload.push(oPayload)
        })

        http
          .post(`${constant.api.DELIVERY_UPDATE}`, aPayload)
          .then(() => {
            _self
              .getView()
              .byId('sSchdeuledDeliveriesTableId')
              .removeSelections()
            MessageToast.show(
              this.getResourceBundle().getText('ScheduledDeliveries.0016')
            )
          })
          .catch((error) => {
            MessageBox.error(error.message)
          })
      },
      getDeliveryHistory() {
        const _self = this
        const aDeliveries = this.handleCreatePayload('')
        http
          .post(`${constant.api.DELIVERY_STATUS}`, aDeliveries)
          .then((data) => {
            data.data[0].deliveryProposals.forEach((oItem) => {
              oItem.fromtime = oItem.deliveryTimeFrom
                .split('T')[1]
                .split('.')[0]
              oItem.totime = oItem.deliveryTimeTo.split('T')[1].split('.')[0]
              oItem.updatetime = oItem.updatedAt.split('T')[1].split('.')[0]
              const sMonth =
                new Date('2022-02-22T23:58:12.336Z').getUTCMonth() + 1 > 9
                  ? new Date('2022-02-22T23:58:12.336Z').getUTCMonth() + 1
                  : `0${new Date('2022-02-22T23:58:12.336Z').getUTCMonth() + 1}`
              oItem.createdDate = `${new Date(
                '2022-02-22T23:58:12.336Z'
              ).getUTCFullYear()}-${sMonth}-${new Date(
                '2022-02-22T23:58:12.336Z'
              ).getUTCDate()}`
            })
            _self
              .getView()
              .getModel('ScheduledDeliveriesModel')
              .setProperty('/history', data.data[0].deliveryProposals)
            // MessageToast.show(
            //   this.getResourceBundle().getText('ScheduledDeliveries.0016')
            // )
          })
          .catch((error) => {
            MessageBox.error(error.message)
          })
      },
    })
)
