sap.ui.define(
  [
    './BaseController',
    'sap/m/Dialog',
    'sap/m/DialogType',
    'sap/m/Button',
    'sap/m/ButtonType',
    'sap/m/Text',
    'sap/m/List',
    'sap/m/StandardListItem',
    'sap/m/ActionListItem',
    'sap/ui/core/Fragment',
    'com/innova/sige/service/http',
    'com/innova/sige/model/constant',
    'sap/ui/model/json/JSONModel',
    'sap/ui/model/Filter',
    'sap/ui/model/FilterOperator',
    'sap/base/util/deepExtend',
  ],
  (
    BaseController,
    Dialog,
    DialogType,
    Button,
    ButtonType,
    Text,
    List,
    StandardListItem,
    ActionListItem,
    Fragment,
    http,
    constant,
    JSONModel,
    Filter,
    FilterOperator,
    deepExtend
  ) =>
    BaseController.extend('com.innova.sige.controller.Main', {
      onInit() {
        this._oRouter = this.getRouter()
        this.getView().byId('DRS2').setVisible(false)
        const oGlobalVHModel = new JSONModel()
        this.getView().setModel(oGlobalVHModel, 'GlobalVHModel')
        this.getView().getModel('GlobalVHModel').setProperty('/VHRecords', [])
        const oListModel = new JSONModel()
        this.getView().setModel(oListModel, 'ListModel')
        const oRouter = this.getOwnerComponent().getRouter()
        oRouter
          .getRoute('Main')
          .attachPatternMatched(this._onObjectMatched, this)
        this.sSelectedVH = ''
        this.clearListData()
      },
      _onObjectMatched() {
        this.clearListData()
        this.getView().byId('sPlantCodeId').setTokens([])
        this.getView().byId('sStorageLocId').setTokens([])
        this.getView().byId('sMaterialId').setTokens([])
        this.getView().byId('sShortText').setValue('')
        this.getView().byId('rbg4').setSelectedIndex(-1)
        this.getView().byId('sShortText').setValue('')
      },
      clearListData() {
        this.getView().getModel('ListModel').setProperty('/VendorData', {
          plantCode: '',
          storageLocation: '',
          materialcode: '',
          materialDescr: '',
        })
      },
      onDetailPress() {
        this._oRouter.navTo('Detail')
      },
      onDateRangeSelect(oEvent) {
        if (oEvent.getSource().getSelectedIndex() === 3) {
          this.getView().byId('DRS2').setVisible(true)
        } else {
          this.getView().byId('DRS2').setVisible(false)
        }
      },
      onSearchPress() {
        if (!this.oApproveDialog) {
          this.oSearchConfirmDialog = new Dialog({
            type: DialogType.Message,
            contentWidth: '39%',
            title: this.getResourceBundle().getText('ListView.0017'),
            titleAlignment: 'Center',
            content: [
              // new Text({
              //   text: this.getResourceBundle().getText('ListView.0018'),
              // }),

              new sap.m.Label({
                text: this.getResourceBundle().getText('ListView.0018'),
                wrapping: true,
                design: 'Bold',
              }),
              new List({
                items: [
                  new ActionListItem({
                    type: 'Inactive',
                    text: this.getResourceBundle().getText('ListView.0014'),
                  }),
                  new ActionListItem({
                    type: 'Inactive',
                    text: this.getResourceBundle().getText('ListView.0015'),
                  }),
                  new ActionListItem({
                    type: 'Inactive',
                    text: this.getResourceBundle().getText('ListView.0016'),
                  }),
                ],
              }),
            ],
            beginButton: new Button({
              type: ButtonType.Emphasized,
              text: this.getResourceBundle().getText('Common.0005'),
              press: function () {
                const oListData = deepExtend(
                  {},
                  this.getView()
                    .getModel('ListModel')
                    .getProperty('/VendorData')
                )
                const aPlantsTokens = this.getView()
                  .byId('sPlantCodeId')
                  .getTokens()
                const aStorageLocTokens = this.getView()
                  .byId('sStorageLocId')
                  .getTokens()
                const aMaterialCodeTokens = this.getView()
                  .byId('sMaterialId')
                  .getTokens()
                const sMaterialDescr = this.getView()
                  .byId('sShortText')
                  .getValue()
                const aPlants = []
                const aStorageLoc = []
                const aMaterials = []
                // const aMaterDescr = [];

                if (aPlantsTokens && aPlantsTokens.length > 0) {
                  aPlantsTokens.forEach((oItem) => {
                    aPlants.push(oItem.getText())
                  })
                }
                if (aStorageLocTokens && aStorageLocTokens.length > 0) {
                  aStorageLocTokens.forEach((oItem) => {
                    aStorageLoc.push(oItem.getText())
                  })
                }
                if (aMaterialCodeTokens && aMaterialCodeTokens.length > 0) {
                  aMaterialCodeTokens.forEach((oItem) => {
                    aMaterials.push(oItem.getText())
                  })
                }
                // if(aMaterialDescrTikens && aMaterialDescrTikens.length > 0){
                //   aMaterialDescrTikens.forEach(oItem => {
                //     aMaterDescr.push(oItem.getText());
                //   });
                // }
                oListData.plantCode = aPlants
                oListData.storageLocation = aStorageLoc
                oListData.materialcode = aMaterials
                oListData.materialDescr = sMaterialDescr
                const oDateFormat = sap.ui.core.format.DateFormat.getInstance({
                  pattern: 'yyyy-MM-dd',
                })
                let oDeliveryToDate = null
                let oDeliveryFromDate = null
                if (this.getView().byId('rbg4').getSelectedIndex() === 0) {
                  oDeliveryToDate = new Date(
                    new Date().setDate(new Date().getDate() + 1)
                  )
                  oDeliveryFromDate = oDateFormat.format(new Date())
                } else if (
                  this.getView().byId('rbg4').getSelectedIndex() === 1
                ) {
                  oDeliveryToDate = new Date(
                    new Date().setDate(new Date().getDate() + 7)
                  )
                  oDeliveryFromDate = oDateFormat.format(new Date())
                } else if (
                  this.getView().byId('rbg4').getSelectedIndex() === 2
                ) {
                  oDeliveryToDate = new Date(
                    new Date().setDate(new Date().getDate() + 30)
                  )
                  oDeliveryFromDate = oDateFormat.format(new Date())
                } else if (
                  this.getView().byId('rbg4').getSelectedIndex() === 3
                ) {
                  oDeliveryToDate = this.getView()
                    .byId('DRS2')
                    .getSecondDateValue()
                  oDeliveryFromDate = this.getView().byId('DRS2').getDateValue()
                  oDeliveryFromDate = oDateFormat.format(oDeliveryFromDate)
                }
                oDeliveryToDate = oDateFormat.format(oDeliveryToDate)
                oDeliveryToDate += 'T00:00:00.000Z'
                oDeliveryFromDate += 'T00:00:00.000Z'
                // oDeliveryToDate = oDateFormat.format(oDeliveryToDate);
                // oDeliveryToDate = oDeliveryToDate.replaceAll('-','/')
                // oDeliveryFromDate = oDeliveryFromDate.replaceAll('-','/')
                if (this.getView().byId('rbg4').getSelectedIndex() === -1) {
                  oDeliveryToDate = null
                  oDeliveryFromDate = null
                }
                this.getOwnerComponent()
                  .getModel('VendorsModel')
                  .setProperty('/searchData', {
                    lifnr: '100045',
                    purchaseDocNumber:
                      this.getView().byId('sPurchaseOrderId').getValue() === ''
                        ? null
                        : this.getView().byId('sPurchaseOrderId').getValue(),
                    shortText: sMaterialDescr,
                    materialNumber: oListData.materialcode,
                    plant: oListData.plantCode,
                    storageLoc: oListData.storageLocation,
                    toDelDate: oDeliveryToDate === '' ? null : oDeliveryToDate,
                    fromDelDate:
                      oDeliveryFromDate === '' ? null : oDeliveryFromDate,
                  })
                this.oSearchConfirmDialog.close()
                this._oRouter.navTo('Detail')
              }.bind(this),
            }),
            endButton: new Button({
              text: this.getResourceBundle().getText('Common.0004'),
              press: function () {
                this.oSearchConfirmDialog.close()
              }.bind(this),
            }),
          })
        }

        this.oSearchConfirmDialog.open()
      },
      onNavBackPress() {
        this._oRouter.navTo('home')
      },
      onPlantVHPress() {
        this.sSelectedVH = 'plant'
        this.getPlantDDValues('100045')
        this.handleValueHelpOpenPress()
      },
      handleValueHelpOpenPress() {
        const oView = this.getView()

        if (!this._pDialog) {
          this._pDialog = Fragment.load({
            id: oView.getId(),
            name: 'com.innova.sige.view.Fragments.GlobalValueHelp',
            controller: this,
          }).then((oDialog) => {
            oDialog.setModel(oView.getModel('GlobalVHModel'), 'GlobalVHModel')
            return oDialog
          })
        }

        this._pDialog.then((oDialog) => {
          oDialog.open()
        })
      },
      getPlantDDValues(sVendor) {
        http
          .get(`${constant.api.PLANT}?lifnr=${sVendor}`)
          .then(this._handlePlantDDValueData.bind(this))
      },
      _handlePlantDDValueData(data) {
        const aValues = []
        data.data.forEach((oItem) => {
          const oRecord = {
            key: oItem.werks,
            text: oItem.name1,
          }
          aValues.push(oRecord)
        })
        this.getView()
          .byId('sGlobalVHSelectId')
          .setTitle(this.getResourceBundle().getText('ListView.0015'))
        this.getView()
          .getModel('GlobalVHModel')
          .setProperty('/VHRecords', aValues)
      },
      onValueHelpSearchPress(oEvent) {
        const sValue = oEvent.getParameter('value')
        const oFilter = new Filter('text', FilterOperator.Contains, sValue)
        const oBinding = oEvent.getParameter('itemsBinding')
        oBinding.filter([oFilter])
      },
      onValueHelpDialogClose(oEvent) {
        // const oSelectedObject = oEvent
        //   .getParameter('selectedItem')
        //   .getBindingContext('GlobalVHModel')
        //   .getObject()
        // // if (this.sSelectedVH === 'plant') {
        //   this.getView()
        //     .getModel('ListModel')
        //     .setProperty('/VendorData/plantCode', oSelectedObject.key)
        // }else if (this.sSelectedVH === 'storage'){
        //   this.getView()
        //     .getModel('ListModel')
        //     .setProperty('/VendorData/storageLocation', oSelectedObject.key)
        // }

        const aSelectedItems = oEvent.getParameter('selectedItems')

        const that = this

        if (aSelectedItems && aSelectedItems.length > 0) {
          aSelectedItems.forEach((oItem) => {
            // const oSelectedObject = oItem.getBindingContext('GlobalVHModel').getObject();
            if (this.sSelectedVH === 'material') {
              const oMultiInput = that.getView().byId('sMaterialId')
              oMultiInput.addToken(
                new sap.m.Token({
                  text: oItem.getDescription(),
                })
              )
              // const oMultiInputText = that.getView().byId("sShortText");
              // if(that.sSelectedVH === 'material'){
              //   oMultiInputText.addToken(new sap.m.Token({
              //     text: oSelectedObject.text
              //   }));
              // }
            } else {
              const oMultiInput = that.getView().byId('sPlantCodeId')
              oMultiInput.addToken(
                new sap.m.Token({
                  text: oItem.getDescription(),
                })
              )
            }
          })
        }
        this._pDialog.then((oDialog) => {
          oDialog.close()
        })
      },
      onStorageLocationVHPress() {
        this.sSelectedVH = 'storage'
        this.getStorageLocationVHData('100045')
        this.handleStorageLocationValueHelpOpenPress()
      },
      handleStorageLocationValueHelpOpenPress() {
        const oView = this.getView()
        if (!this._oStorageLocDialog) {
          this._oStorageLocDialog = Fragment.load({
            id: oView.getId(),
            name: 'com.innova.sige.view.Fragments.StorageLocation',
            controller: this,
          }).then((oDialog) => {
            oDialog.setModel(oView.getModel('GlobalVHModel'), 'GlobalVHModel')
            return oDialog
          })
        }

        this._oStorageLocDialog.then((oDialog) => {
          oDialog.open()
        })
      },
      onStorageLocValueHelpDialogClose(oEvent) {
        // const oSelectedObject = oEvent
        // .getParameter('selectedItem')
        // .getBindingContext('GlobalVHModel')
        // .getObject()
        // this.getView()
        //   .getModel('ListModel')
        //   .setProperty('/VendorData/storageLocation', oSelectedObject.lgort)
        const aSelectedItems = oEvent.getParameter('selectedItems')
        const oMultiInput = this.getView().byId('sStorageLocId')

        if (aSelectedItems && aSelectedItems.length > 0) {
          aSelectedItems.forEach((oItem) => {
            oMultiInput.addToken(
              new sap.m.Token({
                text: oItem.getBindingContext('GlobalVHModel').getObject()
                  .lgort,
              })
            )
          })
        }

        this._oStorageLocDialog.then((oDialog) => {
          oDialog.close()
        })
      },
      getStorageLocationVHData(sVendor) {
        http
          .get(`${constant.api.STORAGELOCATION}?lifnr=${sVendor}`)
          .then(this._handleStorageLocationDDValueData.bind(this))
      },
      _handleStorageLocationDDValueData(data) {
        // const aValues = []
        // data.data.forEach((oItem) => {
        //   const oRecord = {
        //     key: oItem.lgort,
        //     text: oItem.lgobe,
        //   }
        //   aValues.push(oRecord)
        // })
        this.getView()
          .byId('sStorageLocTitleId')
          .setTitle(this.getResourceBundle().getText('ListView.0016'))
        this.getView()
          .getModel('GlobalVHModel')
          .setProperty('/StorageLocation', data.data)
      },
      onFuncLocSearchPress(oEvent) {
        const sValue = oEvent.getParameter('value')
        const oFilter = new Filter('lgort', FilterOperator.Contains, sValue)
        const oBinding = oEvent.getSource().getBinding('items')
        oBinding.filter([oFilter])
      },
      onMaterialValueHelpRequestPress() {
        this.sSelectedVH = 'material'
        this.getMaterialDDValues('100045')
        this.handleValueHelpOpenPress()
      },
      getMaterialDDValues(sVendor) {
        http
          .get(`${constant.api.MATERIAL}?lifnr=${sVendor}`)
          .then(this._handleMaterialDDValueData.bind(this))
      },
      _handleMaterialDDValueData(data) {
        const aValues = []
        data.data.forEach((oItem) => {
          const oRecord = {
            key: oItem.matnr,
            text: oItem.txz01,
          }
          aValues.push(oRecord)
        })
        this.getView()
          .byId('sGlobalVHSelectId')
          .setTitle(this.getResourceBundle().getText('ListView.0019'))
        this.getView()
          .getModel('GlobalVHModel')
          .setProperty('/VHRecords', aValues)
      },
    })
)
