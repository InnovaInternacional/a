sap.ui.define(
  [
    './BaseController',
    'sap/m/Dialog',
    'sap/m/DialogType',
    'sap/m/Button',
    'sap/m/ButtonType',
    'sap/m/Text',
    'sap/m/List',
    'sap/m/StandardListItem',
    'sap/m/ActionListItem',
    'sap/ui/core/Fragment',
    'com/innova/sige/service/http',
    'com/innova/sige/model/constant',
    'sap/ui/model/json/JSONModel',
    'sap/ui/model/Filter',
    'sap/ui/model/FilterOperator',
    'sap/base/util/deepExtend',
  ],
  (
    BaseController,
    Dialog,
    DialogType,
    Button,
    ButtonType,
    Text,
    List,
    StandardListItem,
    ActionListItem,
    Fragment,
    http,
    constant,
    JSONModel
  ) =>
    BaseController.extend('com.innova.sige.controller.CalendarView', {
      onInit() {
        this._oRouter = this.getRouter()

        const oCalendarModel = new JSONModel()
        this.getView().setModel(oCalendarModel, 'CalendarModel')
        const oRouter = this.getOwnerComponent().getRouter()
        oRouter
          .getRoute('CalendarView')
          .attachPatternMatched(this._onObjectMatched, this)
      },
      _onObjectMatched(oEvent) {
        this.sSource = oEvent.getParameter('arguments').source
      },
      onNavBackPress() {
        if (this.sSource === '1') {
          this.getRouter().navTo('Detail')
        } else {
          this.getRouter().navTo('ScheduledDeliveries')
        }
      },
    })
)
