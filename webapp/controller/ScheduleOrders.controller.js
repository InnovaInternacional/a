sap.ui.define(
  [
    './BaseController',
    'sap/m/Dialog',
    'sap/m/DialogType',
    'sap/m/Button',
    'sap/m/ButtonType',
    'sap/m/Text',
    'com/innova/sige/service/http',
    'com/innova/sige/model/constant',
    'sap/ui/model/json/JSONModel',
    'sap/ui/model/Filter',
    'sap/ui/model/FilterOperator',
    'sap/ui/core/Fragment',
    'sap/base/util/deepExtend',
    'sap/m/MessageBox',
  ],
  (
    BaseController,
    Dialog,
    DialogType,
    Button,
    ButtonType,
    Text,
    http,
    constant,
    JSONModel,
    Filter,
    FilterOperator,
    Fragment,
    deepExtend,
    MessageBox
  ) =>
    BaseController.extend('com.innova.sige.controller.ScheduleOrders', {
      onInit() {
        this._oRouter = this.getRouter()
        const oRouter = this.getOwnerComponent().getRouter()
        const oDetailModel = new JSONModel()
        this.getView().setModel(oDetailModel, 'DetailModel')
        const oGlobalVHModel = new JSONModel()
        this.getView().setModel(oGlobalVHModel, 'GlobalVHModel')
        this.getView().getModel('GlobalVHModel').setProperty('/VHRecords', [])
        this.getView().byId('TP6').setInitialFocusedDateValue(new Date())
        oRouter
          .getRoute('Detail')
          .attachPatternMatched(this._onObjectMatched, this)
        this.resetDetailsData()
      },
      resetDetailsData() {
        this.getView().byId('sDriverValue').setValue('')
        const oDetailsRecord = {
          lifnr: '100045',
          msehi: '',
          deliveryDate: '',
          deliveryTimeFrom: '',
          deliveryTimeTo: '',
          isOwnVehicle: false,
          carrier: '',
          carrierEmail: '',
          carrierPhone: '',
          carrierPlates: '',
          eqptId: 1,
          grossWeight: 0,
          netWeight: '',
          notes: '',
          status: '',
          deliveryItem: {},
        }
        this.getView()
          .getModel('DetailModel')
          .setProperty('/DetailRecord', oDetailsRecord)
      },
      _onObjectMatched() {
        // this.handleFieldsEditability()
        this.getView()
          .byId('page')
          .setTitle(this.getResourceBundle().getText('DetailView.0026'))
        this.getView().byId('sCalendarId').setVisible(true)
        this.getView()
          .getModel('DetailModel')
          .setProperty('/isItemsNote', false)
        this.getView().byId('sSaveandSendId').setVisible(true)
        this.getView().byId('sCancelId').setVisible(true)
        this.getView().getModel('DetailModel').setProperty('/mode', true)
        this.resetDetailsData()
        this.getUnloadingData()
        this.getPurchaseRecords()
      },
      getUnloadingData() {
        http
          .get(`${constant.api.UNLOAD}?spras=ES`)
          .then(this._handleUnloadingDDValueData.bind(this))
      },
      _handleUnloadingDDValueData(data) {
        this.getView()
          .getModel('DetailModel')
          .setProperty('/unloadDDData', data.data)
      },
      getPurchaseRecords() {
        const _self = this;
        const oVendorData = this.getOwnerComponent()
          .getModel('VendorsModel')
          .getProperty('/searchData')
        if (oVendorData) {
          // let sDescriptions = '';
          // if(oVendorData.shortText){

          //   for(let i=0;i<oVendorData.shortText.length - 1;i += 1){
          //     sDescriptions += oVendorData.shortText[i] + ' ';
          //   }
          //   sDescriptions += oVendorData.shortText[oVendorData.shortText.length - 1] ;
          // }
          const oPayload = {
            lifnr: oVendorData.lifnr,
            ebeln: oVendorData.purchaseDocNumber,
            txz01: oVendorData.shortText,
            matnr: oVendorData.materialNumber,
            werks: oVendorData.plant,
            lgort: oVendorData.storageLoc,
            eindtTo: oVendorData.toDelDate,
            eindtFrom: oVendorData.fromDelDate,
          }

          // http
          //   .get(
          //     `${constant.api.PURCHASE}?lifnr=${lifnr}&ebeln=${purchaseDocNumber}&txz01=${shortText}&matnr=${materialNumber}&werks=${plant}&lgort=${storageLoc}&eindtTo=${toDelDate}&eindtFrom=${fromDelDate}`
          //   )
          //   .then(this._handlePurchaseOrderData.bind(this))

          http
            .post(`${constant.api.PURCHASE}`, oPayload)
            .then((data) => {
              if (!data.data.length) {
                // MessageBox.information(
                //   this.getResourceBundle().getText('DetailView.0031')
                // )
                MessageBox.information(this.getResourceBundle().getText('DetailView.0031'), {
                  actions: [MessageBox.Action.OK],
                  emphasizedAction: MessageBox.Action.OK,
                  onClose () {
                    const oRouter = _self.getOwnerComponent().getRouter()
                    oRouter.navTo('Main')
                    
                  }
                });
               
              }else{
              this._handlePurchaseOrderData(data)
              }
            })
            .catch((error) => {
              MessageBox.error(error.message)
            })
        }
      },
      _handlePurchaseOrderData(data) {
        const aValues = []
        data.data.forEach((oItem) => {
          oItem.purchaseOrderItems.forEach((oValue) => {
            // eslint-disable-next-line no-param-reassign
            oValue.wemng = oValue.pending_qty
            // eslint-disable-next-line no-param-reassign
            oValue.actual_pending_qty = oValue.pending_qty
            aValues.push(oValue)
          })
        })
        this.getView()
          .getModel('DetailModel')
          .setProperty('/PurchaseRecords', aValues)
      },
      onNavToMasterPage() {
        if (!this.getView().getModel('DetailModel').getProperty('/mode')) {
          this._onObjectMatched()
        } else {
          const oRouter = this.getOwnerComponent().getRouter()
          oRouter.navTo('Main')
        }
      },
      validateMandatoryFields() {
        const aIdValues = [
          'DP1',
          'TP6',
          'sToId',
          'sCarriedId',
          'sPhoneId',
          'sEmailId',
          'sLicenseId',
          'sUOMId',
          'sEquipNoId',
          'sWeightId',
          'sDriverValue',
        ]
        const that = this
        let bValid = true
        aIdValues.forEach((oItem) => {
          if (that.getView().byId(oItem).getValue() === '') {
            bValid = false
            that.getView().byId(oItem).setValueState('Error')
          } else {
            that.getView().byId(oItem).setValueState('None')
          }
        })
        // if(!this.getView().byId("sOwnVehId").getState()){
        //   this.getView().byId("sOwnVehId").getState()
        // }
        if (!bValid) {
          MessageBox.error('Please enter the mandatory fields')
        }
        return bValid
      },
      onSaveAndSendPress() {
        const bValid = this.validateMandatoryFields()
        if (!bValid) {
          return
        }

        if (!this.oApproveDialog) {
          this.oApproveDialog = new Dialog({
            type: DialogType.Message,
            contentWidth: '25%',
            title: this.getResourceBundle().getText('DetailView.0023'),
            titleAlignment: 'Center',
            content: new Text({
              text: this.getResourceBundle().getText('DetailView.0025'),
            }),
            beginButton: new Button({
              type: ButtonType.Emphasized,
              text: this.getResourceBundle().getText('Common.0005'),
              press: function () {
                this.handleSaveoeSend()
                this.oApproveDialog.close()
              }.bind(this),
            }),
            endButton: new Button({
              text: this.getResourceBundle().getText('Common.0004'),
              press: function () {
                this.oApproveDialog.close()
              }.bind(this),
            }),
          })
        }

        this.oApproveDialog.open()
      },
      handleSaveoeSend() {
        const that = this
        const oPayload = deepExtend(
          {},
          this.getView().getModel('DetailModel').getProperty('/DetailRecord')
        )
        const oDateFormat = sap.ui.core.format.DateFormat.getInstance({
          pattern: 'yyyy-MM-dd',
        })

        if (oPayload.deliveryDate) {
          oPayload.deliveryDate = new Date(oPayload.deliveryDate).toISOString()
        }
        if (oPayload.deliveryTimeFrom) {
          const oDate = oDateFormat.format(new Date())
          oPayload.deliveryTimeFrom = new Date(
            `${oDate}T${oPayload.deliveryTimeFrom}Z`
          ).toISOString()
          // oPayload.deliveryTimeFrom = new Date(oPayload.deliveryTimeFrom).toISOString();
        }
        if (oPayload.deliveryTimeTo) {
          const oDate = oDateFormat.format(new Date())
          oPayload.deliveryTimeTo = new Date(
            `${oDate}T${oPayload.deliveryTimeTo}Z`
          ).toISOString()
        }
        const aItems = []
        this.getView()
          .byId('sScheduledOrdersTableId')
          .getSelectedItems()
          .forEach((oItem) => {
            const oSelectedObject = oItem
              .getBindingContext('DetailModel')
              .getObject()
            const oFinalItem = {
              ebeln: oSelectedObject.ebeln,
              ebelp: oSelectedObject.ebelp,
              werks: oSelectedObject.lgort.werks,
              lgort: oSelectedObject.lgort.lgort,
              menge: oSelectedObject.wemng,
            }
            aItems.push(oFinalItem)
          })
        oPayload.deliveryItems = aItems
        oPayload.driver = this.getView().byId('sDriverValue').getValue()
        oPayload.eqptId = parseInt(oPayload.eqptId, 10)
        oPayload.status = '1'
        delete oPayload.deliveryItem
        http
          .post(`${constant.api.DELIVERY}`, oPayload)
          .then((data) => {
            MessageBox.success(
              `${that.getResourceBundle().getText('DetailView.0028')} ${data.data.id
              }`
            )
            that.handleFieldsEditability()
            // MessageBox.success(data.data.message)
          })
          .catch((error) => {
            MessageBox.error(error.message)
          })
      },
      handleFieldsEditability() {
        const aSelectedItems = this.getView()
          .byId('sScheduledOrdersTableId')
          .getSelectedItems()
        const aDeliveredItems = []
        const that = this
        aSelectedItems.forEach((oItem) => {
          const sNotes = that
            .getView()
            .getModel('DetailModel')
            .getProperty('/DetailRecord/notes')
          // eslint-disable-next-line no-param-reassign
          const oSelectedObject = oItem
            .getBindingContext('DetailModel')
            .getObject()
          oSelectedObject.planned_qty =
            parseInt(oSelectedObject.planned_qty, 10) +
            parseInt(oSelectedObject.wemng, 10)
          oSelectedObject.pending_qty =
            parseInt(oSelectedObject.pending_qty, 10) -
            parseInt(oSelectedObject.wemng, 10)
          oSelectedObject.notes = sNotes
          aDeliveredItems.push(oSelectedObject)
        })
        this.getView()
          .getModel('DetailModel')
          .setProperty('/PurchaseRecords', aDeliveredItems)
        this.getView().getModel('DetailModel').setProperty('/isItemsNote', true)
        this.getView().getModel('DetailModel').refresh(true)
        this.getView().byId('sScheduledOrdersTableId').removeSelections()
        // const aIdValues = [
        //   'DP1',
        //   'TP6',
        //   'sToId',
        //   'sCarriedId',
        //   'sPhoneId',
        //   'sEmailId',
        //   'sLicenseId',
        //   'sUOMId',
        //   'sNotesId',
        //   'sOwnVehId',
        //   'sEquipNoId',
        //   'sWeightId',
        //   'sDriverValue',
        // ]
        // aIdValues.forEach((oItem) => {
        //   that.getView().byId(oItem).setEnabled(false)
        // })
        this.getView().getModel('DetailModel').setProperty('/mode', false)
        this.getView().byId('sSaveandSendId').setVisible(false)
        this.getView().byId('sCancelId').setVisible(false)
        this.getView().byId('sCalendarId').setVisible(false)
        this.getView()
          .byId('page')
          .setTitle(this.getResourceBundle().getText('DetailView.0027'))
      },
      onUnitWeightVHPress() {
        this.sSelectedVH = 'plant'
        this.handleValueHelpOpenPress()
        this.getUnitWeightVHValues()
      },
      handleValueHelpOpenPress() {
        const oView = this.getView()

        if (!this._pDialog) {
          this._pDialog = Fragment.load({
            id: oView.getId(),
            name: 'com.innova.sige.view.Fragments.GlobalValueHelp',
            controller: this,
          }).then((oDialog) => {
            oDialog.setModel(oView.getModel('GlobalVHModel'), 'GlobalVHModel')
            return oDialog
          })
        }

        this._pDialog.then((oDialog) => {
          oDialog.open()
        })
      },
      getUnitWeightVHValues() {
        http
          .get(`${constant.api.UNITMEASURE}?spras=EN&dimension=M`)
          .then(this._handleUnitMeasureDDValueData.bind(this))
      },
      _handleUnitMeasureDDValueData(data) {
        const aValues = []
        data.data.forEach((oItem) => {
          const oRecord = {
            key: oItem.msehi.msehi,
            text: oItem.description,
          }
          aValues.push(oRecord)
        })
        this.getView()
          .byId('sGlobalVHSelectId')
          .setTitle(this.getResourceBundle().getText('DetailView.0024'))
        this.getView()
          .getModel('GlobalVHModel')
          .setProperty('/VHRecords', aValues)
      },
      onValueHelpSearchPress(oEvent) {
        const sValue = oEvent.getParameter('value')
        const oFilter = new Filter('text', FilterOperator.Contains, sValue)
        const oBinding = oEvent.getParameter('itemsBinding')
        oBinding.filter([oFilter])
      },
      onValueHelpDialogClose(oEvent) {
        const oSelectedObject = oEvent
          .getParameter('selectedItem')
          .getBindingContext('GlobalVHModel')
          .getObject()
        if (this.sSelectedVH === 'plant') {
          this.getView()
            .getModel('DetailModel')
            .setProperty('/DetailRecord/msehi', oSelectedObject.key)
        }
        this._pDialog.then((oDialog) => {
          oDialog.close()
        })
      },
      onDeliveredQtyChange(oEvent) {
        const sValue = oEvent.getSource().getValue()
        const oSelectedObject = oEvent
          .getSource()
          .getBindingContext('DetailModel')
          .getObject()
        if (
          parseInt(oSelectedObject.actual_pending_qty, 10) <
          parseInt(oEvent.getSource().getValue(), 10)
        ) {
          oEvent.getSource().setValue(sValue.substring(0, sValue.length - 1))
          MessageBox.error(
            this.getResourceBundle().getText('ScheduledDeliveries.0018')
          )
        }
      },
      onDeliverySchedulePress() {
        this._oRouter.navTo('CalendarView', {
          source: '1',
        })
      },
    })
)
