/*!
 * ${copyright}
 */
sap.ui.define(
  [
    '../BaseController',
    '../../model/constant',
    '../../service/http',
    '../../utils/redirect',
    'sap/m/MessageToast',
    'sap/ui/model/json/JSONModel',
    'sap/m/MessageBox',
    'sap/ui/core/Fragment',
    'com/innova/sige/utils/showToast',
  ],
  (
    BaseController,
    constant,
    http,
    redirect,
    MessageToast,
    JSONModel,
    MessageBox,
    Fragment,
    showToast
  ) =>
    /**
     * @class
     * @name Login.controller.js
     * @extends com.innova.sige.controller.BaseController
     * @description - Controlador de la vista del Login
     *
     * @constructor
     * @public
     * @alias com.innova.sige.controller.Login
     *
     * @param {String} sId - id for the new control, generated automatically if no id is given
     * @param {Object} mSettings - initial settings for the new control
     * @returns {void} - No retorna nada.
     *
     * @author Heinner Mayorga <hmayorga@innovainternacional.biz>
     * @version ${version}
     */

    BaseController.extend('com.innova.sige.controller.login.Login', {
      /* =========================================================== */
      /* begin: lifecycle methods                                    */
      /* =========================================================== */

      /**
       * @function
       * @name onInit
       * @description - Se ejecuta cuando se renderiza por primera vez la vista.
       *
       * @private
       * @returns {void} - No retorna nada.
       *
       * @author Heinner Mayorga <hmayorga@innovainternacional.biz>
       * @version ${version}
       *  [FEATURE] Validar el idioma del explorador para mostrar el idioma en el contorl Select.
       */
      onInit() {
        this._oPage = this.byId('panel')
        this.setModel(
          new JSONModel({
            language: window.navigator.language.includes('-')
              ? window.navigator.language.split(/-/)[0].toUpperCase()
              : window.navigator.language.toUpperCase(),
            logoClient: sap.ui.require.toUrl('com/innova/sige/img/logo.png'),
            svgLogo: sap.ui.require.toUrl('com/innova/sige/img/bg1.jpg'),
          })
        )

        this.byId('innovaImg').setSrc(
          sap.ui.require.toUrl('com/innova/sige/img/logo-blanco.png')
        )
        this.getRouter()
          .getRoute('login')
          .attachPatternMatched(this._onRouteMatched, this)
      },

      /* =========================================================== */
      /* finish: lifecycle methods                                   */
      /* =========================================================== */

      /* =========================================================== */
      /* begin: event handlers                                       */
      /* =========================================================== */

      /**
       * @function
       * @name onShowButtonPress
       * @description - Se encarga de cambiar el tipo del sap.m.Input password del formulario.
       *
       * @private
       * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters.
       * @returns {void} - No retorna nada.
       *
       * @author Heinner Mayorga <hmayorga@innovainternacional.biz>
       * @version ${version}
       */
      onShowButtonPress(oEvent) {
        const oSource = oEvent.getSource()
        const oInputPassword = this._getPasswordInputField()
        // @ts-ignore
        if (oSource.getIcon() === 'sap-icon://show') {
          // @ts-ignore
          oSource.setIcon('sap-icon://hide')
          oInputPassword.setType(sap.m.InputType.Text)
        } else {
          // @ts-ignore
          oSource.setIcon('sap-icon://show')
          oInputPassword.setType(sap.m.InputType.Password)
        }
      },

      /**
       * @function
       * @name onHandleSubmit
       * @description - Encargado de enviar los datos al backend.
       *
       * @private
       * @returns {void} - No retorna nada.
       *
       * @author Heinner Mayorga <hmayorga@innovainternacional.biz>
       * @version ${version}
       */
      onHandleSubmit() {
        try {
          const sUserValue = this._getUserInputField().getValue()
          const sPasswordValue = this._getPasswordInputField().getValue()

          if (sUserValue === '' || sPasswordValue === '') {
            const oBundle = this.getResourceBundle()
            this._showMessageError(oBundle.getText('loginState'))
            return
          }

          this.showLoader()
          this._fetchLogin({
            username: sUserValue,
            password: sPasswordValue,
          })
            .then(this._successLoginRequest.bind(this))
            .catch(this._errorLoginRequest.bind(this))
        } catch (error) {
          this.errorHandler(error)
        }
      },

      /**
       * @function
       * @name onShowResetPasswordDialog
       * @description - Muestra el dialogo para enviar el correo y recuperar la contraseña.
       *
       * @private
       * @returns {void} - No retorna nada.
       *
       * @author Heinner Mayorga <hmayorga@innovainternacional.biz>
       * @version ${version}
       */
      onShowResetPasswordDialog() {
        const oView = this.getView()
        Fragment.load({
          id: oView.getId(),
          name: 'com/innova/sige/view/login/fragments/SendEmailReset',
          controller: this,
        }).then((control) => {
          this._oDialog = /** @type {sap.m.Dialog} */ (control)
          oView.addDependent(this._oDialog)
          this._oDialog
            .getEndButton()
            .attachPress(this._oDialog.close.bind(this._oDialog))
          this._oDialog.addStyleClass(
            this.getOwnerComponent().getContentDensityClass()
          )
          this._oDialog.attachAfterClose(
            this._oDialog.destroy.bind(this._oDialog)
          )
          this._oDialog.open()
        })
      },

      /**
       * @function
       * @name onTypeEmail
       * @description - Verifica que en el campo Email este escrito un correo electronico valido
       *
       * @public
       * @returns {void}
       *
       * @author Heinner Mayorga <hmayorga@innovainternacional.biz>
       * @version ${version}
       */
      onTypeEmail(evt) {
        const buttonAdd = this.byId('sendResetEmailButton')
        const email = evt.getSource().getValue()
        const mailregex = /^\w+[\w-+.]*@\w+([-.]\w+)*\.[a-zA-Z]{2,}$/
        if (!mailregex.test(email)) {
          evt.getSource().setValueState(sap.ui.core.ValueState.Warning)
          evt.getSource().setValueStateText('Digite un correo valido')
          buttonAdd.setEnabled(false)
        } else {
          evt.getSource().setValueState(sap.ui.core.ValueState.None)
          buttonAdd.setEnabled(true)
        }
      },

      /**
       * @function
       * @name onShowResetPasswordDialog
       * @description - Pasos a seguir para enviar el correo de recuperación de contraseña.
       *
       * @private
       * @returns {void} - No retorna nada.
       *
       * @author Heinner Mayorga <hmayorga@innovainternacional.biz>
       * @version ${version}
       */
      onSendResetEmail() {
        Promise.resolve(this._oDialog.setBusy(true))
          .then(this._createEmailResetRequest.bind(this))
          .then(http.post.bind(http, `${constant.api.SEND_EMAIL_RESET_PATH}`))
          .then(() => {
            showToast(this.getResourceBundle().getText('Commons.0029'))
            this._oDialog.close()
          })
          .catch(this.errorHandler.bind(this))
          .finally(this._oDialog.setBusy.bind(this._oDialog, false))
      },

      /* =========================================================== */
      /* finish: event handlers                                      */
      /* =========================================================== */

      /* =========================================================== */
      /* begin: internal methods                                     */
      /* =========================================================== */

      /**
       * @function
       * @name _successLoginRequest
       * @description - Se encarga de de recibir el resolve de la promesa del Login Request.
       *
       * @private
       * @param {object} response - Objeto con los datos que retorna el backend.
       * @returns {void} - No retorna nada.
       *
       * @author Heinner Mayorga <hmayorga@innovainternacional.biz>
       * @version ${version}
       */
      _successLoginRequest({ data: { access_token: token, role } }) {
        /* Estable el modelo del Pool de textos para la aplicación */
        const oMainModel = this.getModel('main')

        if (typeof Storage === 'undefined') {
          MessageToast.show(
            'Sorry, your browser does not support web storage...'
          )
          return
        }

        oMainModel.setProperty(
          '/logoAboutInnova',
          sap.ui.require.toUrl('com/innova/sige/img/logo-azul.png')
        )

        oMainModel.setProperty(
          '/logoAboutClient',
          this._oQuery.wc
            ? `/static/${this._oQuery.wc}.png`
            : sap.ui.require.toUrl('com/innova/assets/logo.png')
        )

        oMainModel.setProperty('/role', role)

        oMainModel.refresh(true)
        window.sessionStorage.setItem(
          constant.DEFAULT_SESSION.token,
          `Bearer ${token}`
        )
        window.sessionStorage.setItem(
          constant.DEFAULT_SESSION.url,
          window.location.href
        )

        this._getUserInputField().setValue('')
        this._getPasswordInputField().setValue('')
        this._getMessageStripError().setVisible(false)

        redirect(`${window.location.href.split('#').reverse()[1]}`)
      },

      /**
       * @function
       * @name _fetchLogin
       * @description - Realiza la petición a la api para el login
       *
       * @private
       * @param {Object} username - Usuario.
       * @param {Object} password - Contraseña.
       * @returns {Promise} - No retorna nada.
       *
       * @author Heinner Mayorga <hmayorva@innovainternacional.biz>
       * @version ${version}
       *  * Se elimina la dependencia de JQuery.
       */
      _fetchLogin({ username, password }) {
        return http
          .login(constant.api.VENDOR_LOGIN, {
            email: username,
            password,
          })
          .then((response) => response.data)
          .catch(this.errorHandler.bind(this))
      },

      /**
       * @function
       * @name _showMessageError
       * @description - Muestra un error en la pantalla
       *
       * @private
       * @param {object} vError - Objeto error.
       * @returns {void} - No retorna nada.
       *
       * @author Heinner Mayorga <hmayorga@innovainternacional.biz>
       * @version ${version}
       *  * Se elimina la dependencia de JQuery.
       */
      _showMessageError(vError) {
        // const oMStrip = this._getMessageStripError()
        if (vError.response) {
          MessageBox.show(
            this.getModel('i18n')
              .getResourceBundle()
              .getText(`Error.${vError.response.description.code}`),
            {
              icon: sap.m.MessageBox.Icon.ERROR,
              title: this.getModel('i18n')
                .getResourceBundle()
                .getText(`Error.${vError.status}`),
              actions: [sap.m.MessageBox.Action.CLOSE],
              id: 'messageBoxId2',
              styleClass: this.getOwnerComponent().getContentDensityClass(),
              contentWidth: '100px',
            }
          )
        }
        // oMStrip.setText(vError.message || vError)
        // oMStrip.setVisible(true)
      },

      /**
       * @function
       * @name _getButton
       * @description - Obtiene una única instancia del elemento sap.m.MessageStrip de la vista.
       *
       * @private
       * @returns {sap.m.MessageStrip} - MessageStrip de la vista.
       *
       * @author Heinner Mayorga <hmayorga@innovainternacional.biz>
       * @version ${version}
       */
      _getMessageStripError() {
        if (!this._oMessageStripError) {
          this._oMessageStripError = this.getView().byId('messageError')
        }
        return this._oMessageStripError
      },

      /**
       * @function
       * @name _getUserInputField
       * @description - obtiene una única instancia del elemento sap.m.Input del formulario de la vista.
       *
       * @private
       * @returns {sap.m.Input} - Input de tipo texto del formulario de la vista.
       *
       * @author Heinner Mayorga <hmayorga@innovainternacional.biz>
       * @version ${version}
       */
      _getUserInputField() {
        if (!this._oUserInputField) {
          this._oUserInputField = this.getView().byId('userInputField')
        }
        return this._oUserInputField
      },
      /**
       * @function
       * @name _getPasswordInputField
       * @description - obtiene una única instancia del elemento sap.m.Input del formulario de la vista.
       *
       * @private
       * @returns {sap.m.Input} - Input de tipo password del formulario de la vista.
       *
       * @author Heinner Mayorga <hmayorga@innovainternacional.biz>
       * @version ${version}
       */
      _getPasswordInputField() {
        if (!this._oPasswordInputField) {
          this._oPasswordInputField = this.getView().byId('passInputField')
        }
        return this._oPasswordInputField
      },

      /**
       * @function
       * @name _onRouteMatched
       * @description - Se ejecuta cuando se navega a la vista.
       *
       * @private
       * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters.
       * @returns {void} - No retorna nada.
       *
       * @author Heinner Mayorga <hmayorga@innovainternacional.biz>
       * @version ${version}
       */
      _onRouteMatched(oEvent) {
        const oArgs = oEvent.getParameter('arguments')
        this._oQuery = oArgs['?query'] || {}
        if (this._oQuery.wc) {
          fetch(`static/${this._oQuery.wc}.png`).then(({ ok }) => {
            if (ok) {
              this.getModel().setProperty(
                '/logoClient',
                `static/${this._oQuery.wc}.png`
              )
            }
          })
        }
      },

      /**
       * @function
       * @name _errorLoginRequest
       * @description - Se encarga de de recibir el reject de la promesa del Login Request.
       *
       * @private
       * @param {object} error - Objeto con los datos del error.
       * @returns {void} - No retorna nada.
       *
       * @author Heinner Mayorga <hmayorga@innovainternacional.biz>
       * @version ${version}
       */
      _errorLoginRequest(error) {
        if (error.response) {
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          const oError = error.response.data
          this._showMessageError(oError)
        }
        this.hideLoader(1)
      },

      /**
       * @function
       * @name _createEmailResetRequest
       * @description - Obtiene el email ingresado y crea el objeto a enviar en la petición.
       *
       * @private
       * @returns {Object} - data
       *
       * @author Heinner Mayorga <hmayorga@innovainternacional.biz>
       * @version ${version}
       */
      _createEmailResetRequest() {
        const data = {
          email: this.byId('oIEmailReset').getValue(),
        }
        return data
      },
      /* =========================================================== */
      /* finish: internal methods                                     */
      /* =========================================================== */
    })
)
