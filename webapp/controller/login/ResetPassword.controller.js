/*!
 * ${copyright}
 */
sap.ui.define(
  [
    '../BaseController',
    'com/innova/sige/utils/isEmpty',
    'com/innova/sige/service/http',
    'com/innova/sige/utils/showToast',
    'com/innova/sige/model/constant',
    'sap/ui/model/json/JSONModel',
  ],
  (BaseController, isEmpty, http, showToast, constant, JSONModel) =>
    /**
     * @class
     * @name ResetPassword.controller.js
     * @extends com.innova.sige.controller.BaseController
     * @description - Controlador de la vista de Restablecer Contraseña
     *
     * @constructor
     * @public
     * @alias com.innova.sige.controller.ResetPassword
     * @returns {void} - No retorna nada.
     *
     * @author Heinner Mayorga <hmayorga@innovainternacional.biz>
     * @version ${version}
     */

    BaseController.extend('com.innova.sige.controller.login.ResetPassword', {
      /* =========================================================== */
      /* begin: lifecycle methods                                    */
      /* =========================================================== */

      /**
       * @function
       * @name onInit
       * @description - Se ejecuta cuando se renderiza por primera vez la vista.
       *
       * @private
       * @returns {void} - No retorna nada.
       *
       * @author Heinner Mayorga <hmayorga@innovainternacional.biz>
       * @version ${version}
       *  [FEATURE] Validar el idioma del explorador para mostrar el idioma en el contorl Select.
       */
      onInit() {
        this._oPage = this.byId('panelReset')
        this.setModel(
          new JSONModel({
            language: window.navigator.language.includes('-')
              ? window.navigator.language.split(/-/)[0].toUpperCase()
              : window.navigator.language.toUpperCase(),
            logoClient: sap.ui.require.toUrl('com/innova/sige/img/logo.png'),
            svgLogo: sap.ui.require.toUrl('com/innova/sige/img/bg1.jpg'),
          })
        )

        this.byId('innovaImg').setSrc(
          sap.ui.require.toUrl('com/innova/sige/img/logo-blanco.png')
        )
        this.getRouter()
          .getRoute('reset_password')
          .attachPatternMatched(this._onRouteMatched, this)
      },

      /* =========================================================== */
      /* finish: lifecycle methods                                   */
      /* =========================================================== */

      /* =========================================================== */
      /* begin: event handlers                                       */
      /* =========================================================== */

      /**
       * @function
       * @name onResetPassword
       * @description - Se ejecuta al oprimir el boton de restablecer contraseña.
       *
       * @private
       * @returns {void} - No retorna nada.
       *
       * @author Heinner Mayorga <hmayorga@innovainternacional.biz>
       * @version ${version}
       */
      onResetPassword() {
        Promise.resolve(this._oPage.setBusy(true))
          .then(this._validatePasswords.bind(this))
          .then(http.reset.bind(http, `${constant.api.VENDOR_RESET}`))
          .then(() => {
            showToast(this._i18nProperties.getText('Commons.0035'))
            this.getRouter().navTo('login')
          })
          .catch(this.errorHandler.bind(this))
          .finally(this._oPage.setBusy.bind(this._oPage, false))
      },

      /* =========================================================== */
      /* finish: event handlers                                      */
      /* =========================================================== */

      /* =========================================================== */
      /* begin: internal methods                                     */
      /* =========================================================== */

      /**
       * @function
       * @name _onRouteMatched
       * @description - Se ejecuta cuando se navega a la vista.
       *
       * @private
       * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters.
       * @returns {void} - No retorna nada.
       *
       * @author Heinner Mayorga <hmayorga@innovainternacional.biz>
       * @version ${version}
       */
      _onRouteMatched(oEvent) {
        const oArgs = oEvent.getParameter('arguments')
        this._i18nProperties = this.getModel('i18n').getResourceBundle()
        this._oQuery = oArgs['?query'] || {}
        if (this._oQuery.wc) {
          fetch(`static/${this._oQuery.wc}.png`).then(({ ok }) => {
            if (ok) {
              this.getModel().setProperty(
                '/logoClient',
                `static/${this._oQuery.wc}.png`
              )
            }
          })
        }
      },

      /**
       * @function
       * @name _validatePasswords
       * @description - Valida que los campos no esten vacíos y que las contraseñas esten iguales.
       *
       * @private
       * @returns {Object} - No retorna nada.
       *
       * @author Heinner Mayorga <hmayorga@innovainternacional.biz>
       * @version ${version}
       */
      _validatePasswords() {
        const password = this.byId('oINewPassword').getValue()
        const confirmNewPassword = this.byId('oIConfirmNewPassword').getValue()
        const otp = this.byId('oIOTP').getValue()
        if (isEmpty(password) || isEmpty(confirmNewPassword) || isEmpty(otp)) {
          throw new Error(this._i18nProperties.getText('loginState'))
        }
        if (password !== confirmNewPassword) {
          throw new Error(this._i18nProperties.getText('Commons.0034'))
        }
        return {
          token: this._oQuery.state,
          password,
          otp,
        }
      },

      /* =========================================================== */
      /* finish: internal methods                                     */
      /* =========================================================== */
    })
)
