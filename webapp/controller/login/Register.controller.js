/*!
 * ${copyright}
 */
sap.ui.define(
  [
    '../BaseController',
    '../../utils/keyBy',
    'com/innova/sige/model/process/TypesDocEnum',
    'com/innova/sige/utils/isEmpty',
    'com/innova/sige/service/http',
    'com/innova/sige/utils/showToast',
    'com/innova/sige/model/constant',
    'com/innova/sige/model/process/Vendor',
    'sap/ui/model/json/JSONModel',
    'sap/ui/core/Fragment',
    'sap/m/MessageBox',
  ],
  (
    BaseController,
    keyBy,
    TypesDocEnum,
    isEmpty,
    http,
    showToast,
    constant,
    Vendor,
    JSONModel,
    Fragment,
    MessageBox
  ) =>
    /**
     * @class
     * @name Register.controller.js
     * @extends com.innova.sige.controller.BaseController
     * @description - Controlador de la vista del Registro del proveedor
     *
     * @constructor
     * @public
     * @alias com.innova.sige.controller.Login
     *
     * @param {String} sId - id for the new control, generated automatically if no id is given
     * @param {Object} mSettings - initial settings for the new control
     * @returns {void} - No retorna nada.
     *
     * @author Heinner Mayorga <hmayorga@innovainternacional.biz>
     * @version ${version}
     */

    BaseController.extend('com.innova.sige.controller.login.Register', {
      /* =========================================================== */
      /* begin: lifecycle methods                                    */
      /* =========================================================== */

      /**
       * @function
       * @name onInit
       * @description - Se ejecuta cuando se renderiza por primera vez la vista.
       *
       * @private
       * @returns {void} - No retorna nada.
       *
       * @author Heinner Mayorga <hmayorga@innovainternacional.biz>
       * @version ${version}
       *  [FEATURE] Validar el idioma del explorador para mostrar el idioma en el contorl Select.
       */
      onInit() {
        this._oPage = this.byId('panelRegister')
        this.getRouter()
          .getRoute('sign_up')
          .attachPatternMatched(this._onRouteMatched, this)
        this.setModel(
          new JSONModel({
            emails: [],
            phones: [],
            categories: [],
          }),
          'vendors'
        )
        this.setModel(
          new JSONModel({
            logoClient: sap.ui.require.toUrl(
              'com/innova/sige/img/logoAcerias.jpg'
            ),
          })
        )
      },
      onAfterRendering() {},

      /* =========================================================== */
      /* finish: lifecycle methods                                   */
      /* =========================================================== */

      /* =========================================================== */
      /* begin: event handlers                                       */
      /* =========================================================== */

      /**
       * @function
       * @name onTypeEmail
       * @description - Verifica que en el campo Email este escrito un correo electronico valido
       *
       * @public
       * @returns {void}
       *
       * @author Heinner Mayorga <hmayorga@innovainternacional.biz>
       * @version ${version}
       */
      onTypeEmail(evt) {
        const buttonAdd = this.byId('saveVendorButton')
        const email = evt.getSource().getValue()
        const mailregex = /^\w+[\w-+.]*@\w+([-.]\w+)*\.[a-zA-Z]{2,}$/
        if (!mailregex.test(email)) {
          evt.getSource().setValueState(sap.ui.core.ValueState.Warning)
          evt.getSource().setValueStateText('Digite un correo valido')
          buttonAdd.setEnabled(false)
        } else {
          evt.getSource().setValueState(sap.ui.core.ValueState.None)
          buttonAdd.setEnabled(true)
        }
      },

      /**
       * @function
       * @name onSaveVendorData
       * @description - Muestra el dialogo para el ingreso del otp y las contraseñas
       *
       * @public
       * @returns {void}
       *
       * @author Heinner Mayorga <hmayorga@innovainternacional.biz>
       * @version ${version}
       */
      onSaveVendorData() {
        Promise.resolve(this._oPage.setBusy(false))
          .then(this._validateFieldsRegister.bind(this))
          .then(this._buildRegisterRequest.bind(this))
          .then(
            http.register.bind(http, `${constant.api.VENDOR_REGISTER}`, this._oQuery.state)
          )
          .then(() => {
            showToast(this.getResourceBundle().getText('Commons.0021'))
            this.getRouter().navTo('login')
          })
          .catch(this.errorHandler.bind(this))
          .finally(this._oPage.setBusy.bind(this, false))
      },

      /* =========================================================== */
      /* finish: event handlers                                      */
      /* =========================================================== */

      /* =========================================================== */
      /* begin: internal methods                                     */
      /* =========================================================== */

      /**
       * @function
       * @name _showRefreshTokenDialog
       * @description - Muestra el dialogo para refrezcar el token de registro
       *
       * @public
       * @returns {void}
       *
       * @author Heinner Mayorga <hmayorga@innovainternacional.biz>
       * @version ${version}
       */
      _showRefreshTokenDialog() {
        MessageBox.warning(
          'Su token ha expirado, ¿Desea recibir otro link de registro?',
          {
            actions: [MessageBox.Action.OK, MessageBox.Action.CANCEL],
            emphasizedAction: MessageBox.Action.OK,
            onClose: (sAction) => {
              if (sAction === 'OK') {
                Promise.resolve(
                  http.post(`${constant.api.REFRESH_TOKEN_PATH}`, {
                    token: this._oQuery.state,
                  })
                )
                  .then(() => {
                    showToast(this.getResourceBundle().getText('0081'))
                    this.getRouter().navTo('login')
                  })
                  .catch(this.errorHandler.bind(this))
              } else {
                this.getRouter().navTo('login')
              }
            },
          }
        )
      },

      /**
       * @function
       * @name _getVendorById
       * @description - Obtiene el proveedor por el ID proporcionado por la URL
       *
       * @public
       * @returns {void}
       *
       * @author Heinner Mayorga <hmayorga@innovainternacional.biz>
       * @version ${version}
       */
      _getVendorById() {
        Promise.resolve(this._oPage.setBusy(false))
          .then(
            http.get.bind(
              http,
              `${constant.api.VENDORS_PATH}?lifnr=${this._oQuery.cid}`
            )
          )
          .then(({ data }) => {
            this._oGlobalName = data.name
            const JSONData = new JSONModel(data)
            this.byId('oIEmail').setValue(this._oQuery.email)
            this.setModel(JSONData, 'vendor')
          })
          .catch((oError) => {
            if (oError.response.data.description.code === 'TOKEN_EXPIRED') {
              this._showRefreshTokenDialog()
            } else {
              this.errorHandler(oError)
            }
          })
          .finally(this._oPage.setBusy.bind(this, false))
      },

      /**
       * @function
       * @name _onRouteMatched
       * @description - Se ejecuta cuando se navega a la vista.
       *
       * @private
       * @param {sap.ui.base.Event} oEvent - An Event object consisting of an id, a source and a map of parameters.
       * @returns {void} - No retorna nada.
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      _onRouteMatched(oEvent) {
        this._deletedPhones = []
        const oArgs = oEvent.getParameter('arguments')
        this._oQuery = oArgs['?query'] || {}
        if (isEmpty(this._oQuery.state) || isEmpty(this._oQuery.cid)) {
          this.getRouter().getTargets().display('notFound')
        } else {
          window.sessionStorage.setItem(
            constant.DEFAULT_SESSION.token,
            `Bearer ${this._oQuery.state}`
          )
          this._getVendorById()
          this._oFormModel = new JSONModel({
            attachments: [],
            enableTabs: false,
            evaluationCriteria: [],
            positions: [],
            sumCommercialEval: 0,
            sumTechEval: 0,
            tipoDoc: TypesDocEnum.LIC_COT,
            valueHelp: {
              catProc: [],
              tipoProc: [],
            },
            vendors: [],
          })
          this.setModel(this._oFormModel, 'processModel')
        }
      },

      /**
       * @function
       * @name _validateFieldsRegister
       * @description - valida los campos de contraseña
       *
       * @private
       * @returns {void} -
       *
       * @author Heinner Mayorga <hmayorga@innovainternacional.biz>
       * @version ${version}
       */
      _validateFieldsRegister() {
        const otpInput = this.byId('oIOtp')
        const passwordInput = this.byId('oIPassword')
        const confirmPasswordInput = this.byId('oIConfirmPassword')
        if (isEmpty(otpInput.getValue())) {
          otpInput.setValueState(sap.ui.core.ValueState.Error)
          otpInput.setValueStateText(
            this.getResourceBundle().getText('BiddingProcess.0129')
          )
          throw new Error(
            this.getResourceBundle().getText('BiddingProcess.0129')
          )
        }
        if (isEmpty(passwordInput.getValue())) {
          passwordInput.setValueState(sap.ui.core.ValueState.Error)
          passwordInput.setValueStateText(
            this.getResourceBundle().getText('BiddingProcess.0130')
          )
          throw new Error(
            this.getResourceBundle().getText('BiddingProcess.0130')
          )
        }
        if (isEmpty(confirmPasswordInput.getValue())) {
          confirmPasswordInput.setValueState(sap.ui.core.ValueState.Error)
          confirmPasswordInput.setValueStateText(
            this.getResourceBundle().getText('BiddingProcess.0131')
          )
          throw new Error(
            this.getResourceBundle().getText('BiddingProcess.0131')
          )
        }
        if (passwordInput.getValue() !== confirmPasswordInput.getValue()) {
          confirmPasswordInput.setValueStateText(
            this.getResourceBundle().getText('BiddingProcess.0132')
          )
          throw new Error(
            this.getResourceBundle().getText('BiddingProcess.0132')
          )
        }
      },

      /**
       * @function
       * @name _buildRegisterRequest
       * @description - crea el request para el registro del proveedor
       *
       * @private
       * @returns {Object}
       *
       * @author Heinner Mayorga <hmayorga@innovainternacional.biz>
       * @version ${version}
       */
      _buildRegisterRequest() {
        return {
          otp: this.byId('oIOtp').getValue(),
          password: this.byId('oIPassword').getValue()
        }
      },

      /* =========================================================== */
      /* finish: internal methods                                    */
      /* =========================================================== */
    })
)
