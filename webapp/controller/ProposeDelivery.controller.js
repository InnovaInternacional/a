/* eslint-disable prefer-destructuring */
/* eslint-disable no-param-reassign */
sap.ui.define(
  [
    './BaseController',
    'sap/m/Dialog',
    'sap/m/DialogType',
    'sap/m/Button',
    'sap/m/ButtonType',
    'sap/m/Text',
    'com/innova/sige/service/http',
    'com/innova/sige/model/constant',
    'sap/ui/model/json/JSONModel',

    'sap/m/MessageBox',
    'sap/m/MessageToast',
    'sap/ui/core/Fragment',
  ],
  (
    BaseController,
    Dialog,
    DialogType,
    Button,
    ButtonType,
    Text,
    http,
    constant,
    JSONModel,
    MessageBox
  ) =>
    BaseController.extend('com.innova.sige.controller.ProposeDelivery', {
      onInit() {
        this._oRouter = this.getRouter()
        const oRouter = this.getOwnerComponent().getRouter()
        const oProposeDeliveryModel = new JSONModel()
        this.getView().setModel(oProposeDeliveryModel, 'ProposeDeliveryModel')

        oRouter
          .getRoute('ProposeDelivery')
          .attachPatternMatched(this._onObjectMatched, this)
        //   this.resetDetailsData()
      },
      _onObjectMatched(oEvent) {
        this.getView()
          .getModel('ProposeDeliveryModel')
          .setProperty('/proposerecords', [])
        const sId = oEvent.getParameter('arguments').deliveryid
        this.getDeliveryProposalHistory(sId)
      },
      getDeliveryProposalHistory(sId) {
        const oPayload = {
          lifnr: '100045',
          id: sId ? [sId] : null,
          status: [],
          ebeln: [],
          matnr: [],
          werks: [],
          lgort: [],
          delivery_date_to: null,
          delivery_date_from: null,
        }
        const that = this

        http
          .post(`${constant.api.DELIVERY_SEARCH}`, oPayload)
          .then((data) => {
            that
              .getView()
              .getModel('ProposeDeliveryModel')
              .setProperty('/scheduleddelivery', data.data[0])
            data.data[0].deliveryProposals.forEach((oItem) => {
              oItem.fromtime = oItem.deliveryTimeFrom
                .split('T')[1]
                .split('.')[0]
              oItem.totime = oItem.deliveryTimeTo.split('T')[1].split('.')[0]
              oItem.updatetime = oItem.updatedAt.split('T')[1].split('.')[0]
              const sMonth =
                new Date('2022-02-22T23:58:12.336Z').getUTCMonth() + 1 > 9
                  ? new Date('2022-02-22T23:58:12.336Z').getUTCMonth() + 1
                  : `0${new Date('2022-02-22T23:58:12.336Z').getUTCMonth() + 1}`
              oItem.createdDate = `${new Date(
                '2022-02-22T23:58:12.336Z'
              ).getUTCFullYear()}-${sMonth}-${new Date(
                '2022-02-22T23:58:12.336Z'
              ).getUTCDate()}`
            })
            that
              .getView()
              .getModel('ProposeDeliveryModel')
              .setProperty('/history', data.data[0].deliveryProposals)
          })
          .catch((error) => {
            MessageBox.error(error.message)
          })
      },
      onNavToScheduledDeliveries() {
        this._oRouter.navTo('ScheduledDeliveries')
      },
      onAddPress() {
        const oRecords = {
          deldate: '',
          deltimefrom: '',
          deltimeto: '',
        }
        const aProposedRecords = this.getView()
          .getModel('ProposeDeliveryModel')
          .getProperty('/proposerecords')
        aProposedRecords.push(oRecords)
        this.getView()
          .getModel('ProposeDeliveryModel')
          .setProperty('/proposerecords', aProposedRecords)
      },
      onDeleteProposeDeliveryPress(oEvent) {
        const oSelectedPath = oEvent
          .getSource()
          .getBindingContext('ProposeDeliveryModel')
          .getPath()
        const aProposedRecords = this.getView()
          .getModel('ProposeDeliveryModel')
          .getProperty('/proposerecords')
        aProposedRecords.splice(oSelectedPath.split('/')[2], 1)
        this.getView()
          .getModel('ProposeDeliveryModel')
          .setProperty('/proposerecords', aProposedRecords)
      },
      onProposeDeliverySavePress() {
        const oPayload = this.handleCreateProposeDeliveryPayload()
        http
          .post(`${constant.api.DELIVERY_STATUS}`, oPayload)
          .then(() => {})
          .catch((error) => {
            MessageBox.error(error.message)
          })
      },
      handleCreateProposeDeliveryPayload() {
        const aItems = this.getView().byId('sProposeDelvId').getItems()
        const oScheduleDelivery = this.getView()
          .getModel('ProposeDeliveryModel')
          .getProperty('/scheduleddelivery')
        const aProposals = []
        aItems.forEach((oItem) => {
          const oCurrentItem = oItem
            .getBindingContext('ProposeDeliveryModel')
            .getObject()
          oCurrentItem.notes = null
          aProposals.push(oCurrentItem)
        })
        const oPayload = {
          id: oScheduleDelivery.id,
          status: '1',
          proposals: aProposals,
        }
        return oPayload
      },
    })
)
