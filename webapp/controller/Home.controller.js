sap.ui.define(['./BaseController'], (BaseController) =>
  /**
   * @class
   * @name BaseController.js
   * @extends sap.ui.core.mvc.Controller
   * @description - Controlador del Home
   *
   * @constructor
   * @public
   * @alias com.innova.sige.controller.BaseController
   *
   * @param {String} sId - id for the new control, generated automatically if no id is given
   * @param {Object} mSettings - initial settings for the new control
   * @returns {void} - Noting to return.
   *
   * @author Edwin Valencia <evalencia@innovainternacional.biz>
   * @version ${version}
   */
  BaseController.extend('com.innova.sige.controller.Home', {
    /**
     * @function
     * @name onInit
     * @description - Se ejecuta cuando se renderiza por primera vez la vista.
     *
     * @private
     * @returns {void} - Noting to return.
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    onInit() {
      this._oRouter = this.getRouter()
    },
    onPendingScheduleOrderPress(){
      this._oRouter.navTo("Main")
    },
    onSearchOrdersPress(){
      this._oRouter.navTo("SearchOrders")
    }
  })
)
