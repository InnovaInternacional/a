sap.ui.define(
  ['./BaseController', 'sap/ui/model/json/JSONModel'],
  (BaseController, JSONModel) =>
    BaseController.extend('com.innova.sige.controller.App', {
      /**
       * @function
       * @name onInit
       * @description - Se ejecuta cuando se inicia la aplicación
       *
       * @private
       * @returns {void} - Noting to return.
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      onInit() {
        this.setModel(
          new JSONModel({
            busy: false,
            delay: 0,
          }),
          'appView'
        )
      },
    })
)
