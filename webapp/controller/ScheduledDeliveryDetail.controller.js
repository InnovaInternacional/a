sap.ui.define(
  [
    './BaseController',
    'sap/m/Dialog',
    'sap/m/DialogType',
    'sap/m/Button',
    'sap/m/ButtonType',
    'sap/m/Text',
    'sap/m/List',
    'sap/m/StandardListItem',
    'sap/m/ActionListItem',
    'sap/ui/core/Fragment',
    'com/innova/sige/service/http',
    'com/innova/sige/model/constant',
    'sap/ui/model/json/JSONModel',
    'sap/ui/model/Filter',
    'sap/ui/model/FilterOperator',
    'sap/base/util/deepExtend',
    'sap/m/MessageBox',
  ],
  (
    BaseController,
    Dialog,
    DialogType,
    Button,
    ButtonType,
    Text,
    List,
    StandardListItem,
    ActionListItem,
    Fragment,
    http,
    constant,
    JSONModel,
    MessageBox
  ) =>
    BaseController.extend('com.innova.sige.controller.SearchOrders', {
      onInit() {
        this._oRouter = this.getRouter()
        // this.getView().byId('DRS2').setVisible(false)

        const oDeliveryDetailModel = new JSONModel()
        this.getView().setModel(oDeliveryDetailModel, 'DeliveryDetailModel')
        const oRouter = this.getOwnerComponent().getRouter()
        oRouter
          .getRoute('ScheduledDeliveryDetail')
          .attachPatternMatched(this._onObjectMatched, this)
        this.sSelectedVH = ''
        // this.clearListData()
      },
      _onObjectMatched(oEVent) {
        this.getView()
          .getModel('DeliveryDetailModel')
          .setProperty('/deliverydata', {})
        const sId = oEVent.getParameter('arguments').deliveryid
        this.getDeliveryDetail(sId)
      },
      getDeliveryDetail(sId) {
        const oPayload = {
          lifnr: '100045',
          id: sId ? [sId] : null,
          status: [],
          ebeln: [],
          matnr: [],
          werks: [],
          lgort: [],
          delivery_date_to: null,
          delivery_date_from: null,
        }
        const that = this

        // http
        //   .get(
        //     `${constant.api.PURCHASE}?lifnr=${lifnr}&ebeln=${purchaseDocNumber}&txz01=${shortText}&matnr=${materialNumber}&werks=${plant}&lgort=${storageLoc}&eindtTo=${toDelDate}&eindtFrom=${fromDelDate}`
        //   )
        //   .then(this._handlePurchaseOrderData.bind(this))

        http
          .post(`${constant.api.DELIVERY_SEARCH}`, oPayload)
          .then((data) => {
            that.handleDeliveryDetails(data.data)
          })
          .catch((error) => {
            MessageBox.error(error.message)
          })
      },
      onNavToMasterPage() {
        this._oRouter = this.getRouter()
        this._oRouter.navTo('ScheduledDeliveries')
      },
      handleDeliveryDetails(data) {
        this.getView()
          .getModel('DeliveryDetailModel')
          .setProperty('/deliverydata', data[0])
      },
    })
)
