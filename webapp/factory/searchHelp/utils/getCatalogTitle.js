sap.ui.define(
  [],
  () =>
    /**
     * @function
     * @name getCatalogTitle
     * @description - Obtener titulo del catalogo
     *
     * @public
     * @param {Object} oCatalog - Catalogo
     * @returns {String}
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    ({ SCRTEXT_L, REPTEXT }) =>
      SCRTEXT_L || REPTEXT
)
