sap.ui.define(
  [],
  () =>
    /**
     * @function
     * @name isKeyField
     * @description - Valida si es un campo clave
     *
     * @public
     * @param {Object} oCatalog - Catalogo
     * @returns {Boolean}
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    ({ KEY, KEY_SEL }) =>
      KEY === 'X' || KEY_SEL === 'X'
)
