/*!
 * ${copyright}
 */
sap.ui.define(
  ['./capitalize', './getCatalogTitle', './isKeyField'],
  (capitalize, getCatalogTitle, isKeyField) => ({
    capitalize,
    getCatalogTitle,
    isKeyField,
  })
)
