sap.ui.define([], () => {
  const capitalize = {
    /**
     * @function
     * @name capitalizeFieldname
     * @description - Capitalizar fieldname SAP
     *
     * @public
     * @param {string} sFieldName String a concatenar y ser capitalizado.
     * @returns {string} - string concatenado.
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    fieldname(sFieldName) {
      return sFieldName
        .split(/\s+/)
        .map((w) =>
          w.includes('_')
            ? w
                .split(/_/)
                .map((s) => capitalize.firstLetter(s))
                .join('')
            : capitalize.firstLetter(w)
        )
        .join()
    },
    /**
     * @function
     * @name capitalizeFirstLetter
     * @description - Capitaliza el string pasado.
     *
     * @public
     * @param {string} str - string a convertir.
     * @returns {string} - string formateado.
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    firstLetter(str) {
      return str && str.charAt(0).toUpperCase() + str.slice(1).toLowerCase()
    },
  }

  return capitalize
})
