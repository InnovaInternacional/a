sap.ui.define(
  [
    './utils/index',
    'com/innova/sige/formatter/maxLength',
    'sap/m/Column',
    'sap/m/ColumnListItem',
    'sap/m/Input',
    'sap/m/Text',
    'sap/ui/layout/form/FormElement',
  ],
  (utils, maxLength, Column, ColumnListItem, Input, Text, FormElement) => {
    const factory = {
      /**
       * @function
       * @name sHelpColumnFactory
       * @description - Función Factory para las columnas de la tabla de ayuda de busqueda
       *
       * @public
       * @param {string} key - Campo llave.
       * @returns {function}
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      sHelpColumnFactory(key) {
        return (sId, oContext) =>
          new Column(sId, {
            hAlign: 'Begin',
            popinDisplay: 'Inline',
            header: new Text({
              text: oContext.getProperty('SCRTEXT_L'),
            }),
            visible: oContext.getProperty('TECH') !== 'X',
            minScreenWidth:
              key !== oContext.getProperty('FIELDNAME') ? 'Tablet' : '',
            demandPopin: true,
          })
      },
      /**
       * @function
       * @name sHelpItemsFactory
       * @description - Función Factory para los items de la tabla de ayuda de busqueda
       *
       * @public
       * @param {string} sId - id del control.
       * @param {object} oContext - Contexto del objeto del binding.
       * @returns {sap.m.ColumnListItem} - Noting to return.
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      sHelpItemsFactory(sId, oContext) {
        return new ColumnListItem(`${sId}-ColumnListItem`, {
          type: 'Active',
          unread: false,
          selected: '{selected}',
        }).bindAggregation('cells', '/catalog', (id, context) => {
          const value = oContext.getProperty(context.getProperty('FIELDNAME'))
          return new Text(id, {
            text: value,
          })
        })
      },
      /**
       * @function
       * @name tableItemDynamicSH
       * @description - Factory para los Items de la sap.m.Table de posiciones.
       *
       * @private
       * @param {string} sPath - Path del objeto del binding.
       * @returns {function} - ColumnListItem para la agregación de los items de la sap.m.Table.
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      tableItemDynamicSH(sPath) {
        return (sId, oContext) =>
          new ColumnListItem(`ColumnListItem-${sId}`, {
            type: 'Active',
            selected: '{selected}',
            press: (oSource) => {
              this.searchHelp.onClose.call(this, [
                oSource.getSource().getBindingContext(),
              ])
            },
          }).bindAggregation(
            'cells',
            sPath,
            (id, context) =>
              new Text(id, {
                text: oContext.getProperty(context.getProperty('FIELDNAME')),
              })
          )
      },

      /* =========================================================== */
      /* Begin: filterFields                                         */
      /* =========================================================== */

      /**
       * @function
       * @name filterFields
       * @description - Función Factory del binding del Formulario de Custom filter
       *
       * @private
       * @param {string} sId - id del la columna.
       * @param {object} oContext - Contexto del objeto del binding.
       * @returns {sap.m.VBox} - Columna de la tabla.
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      filterFields(sId, oContext) {
        const oCatalog = oContext.getObject()

        const sTitle = utils.getCatalogTitle(oCatalog)
        const sInttype = oCatalog.INTTYPE

        const id = this.createId(`input${oCatalog.FIELDNAME}`)

        return new FormElement(sId, {
          label: `${sTitle}`,
          fields: [
            sInttype === 'N'
              ? factory._filterFieldTypeN.call(this, id, oCatalog)
              : factory._filterFieldTypeV.call(this, id, oCatalog),
          ],
        })
      },
      /**
       * @function
       * @name _filterFieldTypeN
       * @description - Función Factory los campos de tipo N
       *
       * @private
       * @param {string} id - id del Input.
       * @param {object} catalog - Catalogo del campo
       * @returns {sap.m.VBox} - Columna de la tabla.
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      _filterFieldTypeN(id, catalog) {
        return new Input(id, {
          fieldGroupIds: 'customFilter',
          /*  liveChange: formUtils.onLiveChangeTypeNumberN.bind(this), */
          maxLength: maxLength(catalog.INTLEN),
          name: `${catalog.FIELDNAME}`,
          placeholder: utils.getCatalogTitle(catalog),
          showValueHelp: false,
          type: 'Text',
          change(oEvent) {
            const oSource = oEvent.getSource()
            const sValue = oSource.getValue()

            oSource.setSelectedKey(sValue)
          },
        })
      },
      /**
       * @function
       * @name _filterFieldTypeV
       * @description - Función Factory los campos de tipo V
       *
       * @private
       * @param {string} id - id del Input.
       * @param {object} catalog - Catalogo del campo
       * @returns {sap.m.VBox} - Columna de la tabla.
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      _filterFieldTypeV(id, catalog) {
        const title = utils.getCatalogTitle(catalog)
        return new Input(id, {
          fieldGroupIds: 'customFilter',
          maxLength: maxLength(catalog.INTLEN),
          name: `${catalog.FIELDNAME}`,
          placeholder: title,
          showSuggestion: false,
          showValueHelp: false,
          type: 'Text',
          valueHelpRequest: [this.searchHelp.onValueHelpRequest, this],
          width: '100%',
          change(oEvent) {
            if (catalog.LOWERCASE !== 'X') {
              const oSource = oEvent.getSource()
              let sValue = oEvent.getParameter('value')
              sValue = sValue.toUpperCase()
              oSource.setValue(sValue)
            }
          },
        })
      },

      /* =========================================================== */
      /* finish: filterFields                                     */
      /* =========================================================== */
    }

    return factory
  }
)
