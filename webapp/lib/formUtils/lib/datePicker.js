/*!
 * ${copyright}
 */
sap.ui.define([], () => ({
  /**
   * @function
   * @name getDatePickerData
   * @description - get data from DatePicker
   *
   * @public
   * @param {object} context
   * @param {boolean} context.isVariant - Es variante
   * @param {object} context.field - Control VBox
   * @param {string} context.functionName - Nombre de la función para el backend
   * @param {string} context.group - Grupo al que pertenece el campo
   * @returns {object}
   *
   * @author Edwin Valencia <evalencia@innovainternacional.biz>
   * @version ${version}
   */
  getDatePickerData({ field }) {
    const name = field.getName()
    const value = field.getValue()

    if (value) {
      return { [name]: field.getDateValue().toISOString() }
    }
    return { [name]: null }
  },
  /**
   * @function
   * @name setDatePickerData
   * @description - Set data to DatePicker
   *
   * @private
   * @param {object} context
   * @param {sap.m.DatePicker} context.field - Campo
   * @param {object} context.data - Datos
   * @returns {void} - Noting to return.
   *
   * @author Edwin Valencia <evalencia@innovainternacional.biz>
   * @version ${version}
   */
  setDatePickerData({ field, data }) {
    const name = field.getName()
    const value = data[name]
    field.setDateValue(new Date(value))
  },
  /**
   * @function
   * @name cleanDatePicker
   * @description - Clean DatePicker
   *
   * @private
   * @param {object} context
   * @param {sap.m.DatePicker} context.field - field
   * @returns {void} - Noting to return
   *
   * @author Edwin Valencia <evalencia@innovainternacional.biz>
   * @version ${version}
   */
  cleanDatePicker({ field }) {
    field.setDateValue()
  },
}))
