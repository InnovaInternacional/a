/*!
 * ${copyright}
 */
sap.ui.define(
  ['com/innova/sige/model/variant/ItemVariant', 'sap/ui/core/ListItem'],
  (ItemVariant, ListItem) =>
    /**
     * @function
     * @name getInputData
     * @description - Obtiene el valor del control Input
     *
     * @public
     * @param {object} context
     * @param {boolean} context.isVariant - Es variante
     * @param {object} context.field - Control VBox
     * @param {string} context.functionName - Nombre de la función para el backend
     * @param {string} context.group - Grupo al que pertenece el campo
     * @returns {object}
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    ({
      getInputData({ isVariant, field, functionName, group }) {
        const name = field.getName()
        const value = field.getSelectedKey() || field.getValue() || null

        if (isVariant) {
          return {
            [name]: new ItemVariant({
              fieldname: name,
              function: functionName,
              group,
              low: value,
              tabname: field.data('tabname'),
            }),
          }
        }
        return { [name]: value }
      },
      /**
       * @function
       * @name setInputData
       * @description - Obtener datos del control
       *
       * @private
       * @param {object} context
       * @param {sap.m.Input} context.field - Campo
       * @param {string|object} context.data - Datos
       * @returns {void} - Noting to return.
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      setInputData({ field, data }) {
        const value = data[field.getName()]
        if (field.getShowSuggestion()) {
          field.setSelectedKey(value)
          field.destroySuggestionItems()
          field.fireSuggest({ suggestValue: value })
        } else if (field.getValueHelpOnly()) {
          field.destroySuggestionItems().addSuggestionItem(
            new ListItem({
              key: value,
              text: value,
            })
          )
          field.setSelectedKey(value)
          field.fireChangeEvent(value)
        } else {
          field.setValue(value)
        }
      },
      /**
       * @function
       * @name cleanMultiInput
       * @description - Limpiar control
       *
       * @private
       * @param {object} context
       * @param {sap.m.Input} context.field - Campo
       * @returns {void} - Noting to return.
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      cleanInput({ field }) {
        field.destroySuggestionItems()
        field.setSelectedKey(undefined)
        field.setValue(undefined)
      },
      /**
       * @function
       * @name isInputValid
       * @description - Valida si el valor del control es valido
       *
       * @public
       * @param {object} context
       * @param {sap.m.Input} context.field - Campo
       * @returns {boolean}
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      isInputValid({ field }) {
        if (field.getShowSuggestion() || field.getValueHelpOnly()) {
          return !!field.getSelectedKey()
        }
        return !!field.getValue()
      },
    })
)
