/*!
 * ${copyright}
 */
sap.ui.define(
  [
    'com/innova/sige/model/item/FormItem',
    'com/innova/sige/model/item/OptionType',
    'com/innova/sige/model/variant/ItemVariant',
    'com/innova/sige/utils/addPatternToText',
    'sap/m/Token',
    'sap/ui/core/ValueState',
  ],
  (FormItem, OptionType, ItemVariant, addPatternToText, Token, ValueState) => {
    /**
     * @function
     * @name validateTokenExists
     * @description - Valida si ya existe la propiedad .
     *
     * @public
     * @param {object[]} aToken - Arreglo de los tokens que se encuentran en el MultiIpunt
     * @param {string} sData - Data a validar
     * @returns {boolean} - si existe retorna true.
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    const validateTokenExists = (aToken, sData) => {
      const iTokenLength = aToken.length

      if (iTokenLength > 0) {
        /*  aToken.some((token) => token.getKey() === sData) */
        for (let i = 0; i < iTokenLength; i += 1) {
          const oToken = aToken[i]
          if (oToken.getKey() === sData) {
            return true
          }
        }
      }

      return false
    }

    /**
     * @function
     * @name validatorMultiInput
     * @description - Retorna un Closure de JS para ser agregado a un MultiInput.
     *  Agrega el validador del MultiInput de la vista.
     *
     * @public
     * @param {string} sLowerCase - parametro que indica si el texto debe ser Mayúscula o no.
     * @returns {function} - Closure con el validador del MultiInput.
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    const validatorMultiInput =
      (sLowerCase) =>
      // closure de JavaScript
      (args) => {
        let { text } = args
        if (sLowerCase !== 'X') {
          text = text.toUpperCase()
        }
        return new Token({
          key: text,
          text,
        })
      }

    /**
     * @function
     * @name validatorEmailMultiInput
     * @description - Validar las entradas de correo
     *
     * @public
     * @param {object} args - Argumento de entrada
     * @returns {sap.m.Token|undefined}
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    const validatorEmailMultiInput = (args) => {
      const { text } = args
      if (/^\w+([\\.-]?\w+)*@\w+([\\.-]?\w+)*(\.\w{2,3})+$/.test(text)) {
        return new Token({
          key: text,
          text,
        })
      }
      return undefined
    }

    /**
     * @function
     * @name emailTokenUpdate
     * @description - Maneja la actualización de los token de correo
     *
     * @public
     * @param {sap.ui.base.Event} oEvent An Event object consisting of an id, a source and a map of parameters.
     * @returns {void}
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    const emailTokenUpdate = (oEvent) => {
      const oSource = /** @type {sap.m.MultiInput}  */ (oEvent.getSource())
      const oContext = /** @type {sap.ui.model.Context}  */ (
        oSource.getBindingContext()
      )
      const sPath = oContext.getPath()
      const oModel = /** @type {sap.ui.model.json.JSONModel} */ (
        oContext.getModel()
      )
      let aEmail = oContext.getProperty('E_MAILS')

      const sType = oEvent.getParameter('type')
      const aAddedTokens = oEvent.getParameter('addedTokens')
      const aRemovedTokens = oEvent.getParameter('removedTokens')

      // add new context to the data of the model, when new token is being added
      if (sType === 'added') {
        aAddedTokens.forEach((oToken) => {
          aEmail.push({
            EMAIL: oToken.getKey(),
          })
        })
      } else if (sType === 'removed') {
        // remove contexts from the data of the model, when tokens are being removed
        aRemovedTokens.forEach((oToken) => {
          aEmail = aEmail.filter((context) => context.EMAIL !== oToken.getKey())
        })
      }

      oModel.setProperty(`${sPath}/E_MAILS`, aEmail)
    }

    /**
     * @function
     * @name isMultiInputValid
     * @description - Valida si el valor del control es valido
     *
     * @public
     * @param {object} context
     * @param {sap.m.MultiInput} context.field - Campo
     * @returns {boolean}
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    const isMultiInputValid = ({ field }) => field.getTokens().length > 0

    return {
      isMultiInputValid,
      /** .
       * @function
       * @name addTokensToMultiInput
       * @description - Valida si ya existe la propiedad .
       *
       * @public
       * @param {object[]} array - Información que debe agregarse al MultiInput
       * @param {string} sKey - Attributo del objeto
       * @param {sap.m.MultiInput} oElement - MultiInput al cual se agregara la data
       * @returns {void} - Noting to return.
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      addTokensToMultiInput(array, sKey, oElement) {
        array.forEach((element) => {
          let sData = element[sKey] || element.getObject()[sKey]
          sData = sData.replace(/\*/g, '')
          if (!validateTokenExists(oElement.getTokens(), sData)) {
            oElement.addToken(
              new Token({
                key: sData,
                text: sData,
              })
            )
          }
        })
      },

      /**
       * @function
       * @name addValidatorToMultiInputs
       * @description - Agregar validador a MultiInputs de la vista
       *
       * @public
       * @param {object} oContext - Objecto contexto
       * @param {object[]} oContext.fields - Vista
       * @returns {void} - No retorna nada
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      addValidatorAllMultiInputs({ fields }) {
        fields
          .filter((oControl) => oControl.isA('sap.m.MultiInput'))
          .forEach((oMultiInput) => {
            oMultiInput.removeAllValidators()
            oMultiInput.addValidator(
              validatorMultiInput(oMultiInput.data('lowercase'))
            )
          })
      },
      /**
       * @function
       * @name addValidatorEmailMultiInputs
       * @description - Agregar validador a MultiInputs de correos
       *
       * @public
       * @param {object} oContext - Objecto contexto
       * @param {object[]} oContext.fields - Controles de la vista
       * @returns {void} - No retorna nada
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      addValidatorEmailMultiInputs({ fields }) {
        fields
          .filter((oControl) => oControl.isA('sap.m.MultiInput'))
          .forEach((oMultiInput) => {
            oMultiInput.removeAllValidators()
            oMultiInput.addValidator(validatorEmailMultiInput)
            oMultiInput.attachTokenUpdate(emailTokenUpdate)
          })
      },
      /**
       * @function
       * @name getMultiInputData
       * @description - Obtener datos del control VBox
       *
       * @private
       * @param {object} context
       * @param {sap.m.MultiInput} context.field - Campo
       * @param {string} context.functionName - Nombre de la función para el backend
       * @param {string} context.group - Grupo al que pertenece el campo
       * @param {boolean} context.isVariant - Es variante
       * @returns {object}
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      getMultiInputData({ field }) {
        let aItem = []
        const tokens = field.getTokens()
        if (isMultiInputValid({ field })) {
          field.setValueState(ValueState.None)
          aItem = tokens.map((token) => token.getKey())
        }
        return { ...(aItem.length && { [field.getName()]: aItem }) }
      },
      /**
       * @function
       * @name setMultiInputData
       * @description - Set MultiInput data
       *
       * @private
       * @param {object} context
       * @param {sap.m.MultiInput} context.field - field
       * @param {object} context.data - data
       * @returns {void} - Noting to return
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      setMultiInputData({ field, data }) {
        const value = data[field.getName()]
        field.setTokens(
          value.map((item) => new Token({ key: item.key, text: item.text }))
        )
      },
      /**
       * @function
       * @name cleanMultiInput
       * @description - Clean MultiInput
       *
       * @private
       * @param {object} context
       * @param {sap.m.MultiInput} context.field - field
       * @returns {void} - Noting to return
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      cleanMultiInput({ field }) {
        field.removeAllTokens()
        field.fireTokenUpdate({ type: 'removed' })
      },
    }
  }
)
