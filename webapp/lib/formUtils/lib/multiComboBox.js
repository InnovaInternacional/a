/*!
 * ${copyright}
 */
sap.ui.define([], () => ({
  /**
   * @function
   * @name getMultiComboBoxData
   * @description - Obtiene el valor del control ComboBox
   *
   * @public
   * @param {object} context
   * @param {sap.m.MultiComboBox} context.field - Control
   * @returns {object}
   *
   * @author Edwin Valencia <evalencia@innovainternacional.biz>
   * @version ${version}
   */
  getMultiComboBoxData({ field }) {
    const keys = field.getSelectedKeys()
    return keys.length && { [field.getName()]: field.getSelectedKeys() }
  },
  /**
   * @function
   * @name setMultiComboBoxData
   * @description - Set MultiComboBox data.
   *
   * @public
   * @param {object} context
   * @param {sap.m.MultiComboBox} context.field - Campo
   * @param {string|object} context.data - Datos
   * @returns {void} - Noting to return.
   *
   * @author Edwin Valencia <evalencia@innovainternacional.biz>
   * @version ${version}
   */
  setMultiComboBoxData({ field, data }) {
    field.setSelectedKeys(data[field.getName()].map(({ key }) => key))
  },
  /**
   * @function
   * @name isMultiComboBoxValid
   * @description - Is MultiComboBox valid.
   *
   * @public
   * @param {object} context
   * @param {sap.m.MultiComboBox} context.field - Campo
   * @returns {boolean}
   *
   * @author Edwin Valencia <evalencia@innovainternacional.biz>
   * @version ${version}
   */
  isMultiComboBoxValid({ field }) {
    return field.getSelectedItems().length > 0
  },
  /**
   * @function
   * @name cleanMultiInput
   * @description - Clean MultiComboBox.
   *
   * @public
   * @param {object} context
   * @param {sap.m.MultiComboBox} context.field - field
   * @returns {void} - Noting to return.
   *
   * @author Edwin Valencia <evalencia@innovainternacional.biz>
   * @version ${version}
   */
  cleanMultiComboBox({ field }) {
    field.resetProperty('selectedKeys')
  },
}))
