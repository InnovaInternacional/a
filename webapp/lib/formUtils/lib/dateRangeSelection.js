/*!
 * ${copyright}
 */
sap.ui.define(
  [
    'com/innova/sige/model/item/FormItem',
    'com/innova/sige/model/item/OptionType',
    'com/innova/sige/model/variant/ItemVariant',
    'com/innova/sige/utils/formatDate',
    'com/innova/vendor/moment',
  ],
  (FormItem, OptionType, ItemVariant, formatDate, moment) => {
    /**
     * @function
     * @name getDateRangeSelectionData
     * @description - Obtiene los datos de un control DateRangeSelectionData.
     *
     * @public
     * @param {object} context - Objeto contexto
     * @param {boolean} context.isVariant - Es variante
     * @param {sap.m.DateRangeSelection} context.field - Control VBox
     * @param {string} context.functionName - Nombre de la función para el backend
     * @param {string} context.group - Grupo al que pertenece el campo
     * @param {object} context.value - Valor
     * @returns {object}
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    // eslint-disable-next-line no-unused-vars
    const buildItem = ({ isVariant, field, functionName, group, value }) =>
      isVariant
        ? new ItemVariant({
            fieldname: field.getName(),
            function: functionName,
            group,
            high: formatDate(field.getSecondDateValue() || value),
            low: formatDate(value),
            tabname: field.data('tabname'),
          })
        : new FormItem({
            fieldname: field.getName(),
            high: formatDate(field.getSecondDateValue() || value),
            low: formatDate(value),
            option: OptionType.BT,
            tabname: field.data('tabname'),
          })

    return {
      /**
       * @function
       * @name getDateRangeSelectionData
       * @description - Obtiene los datos de un control DateRangeSelectionData.
       *
       * @public
       * @param {object} context - Objeto contexto
       * @param {boolean} context.isVariant - Es variante
       * @param {sap.m.DateRangeSelection} context.field - Control VBox
       * @param {string} context.functionName - Nombre de la función para el backend
       * @param {string} context.group - Grupo al que pertenece el campo
       * @returns {object}
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      getDateRangeSelectionData({ field }) {
        const value = /** @type {Date} */ (field.getDateValue())
        let oItem
        if (value) {
          const afterValue = /** @type {Date} */ (field.getSecondDateValue())

          oItem = `${value.toISOString()}_${afterValue.toISOString()}`
          /*  oItem = buildItem({ isVariant, field, functionName, group, value }) */
        }
        return { ...(oItem && { [field.getName()]: oItem }) }
      },
      /**
       * @function
       * @name setDateRangeSelectionData
       * @description - Obtener datos del control DateRangeSelection
       *
       * @private
       * @param {object} context
       * @param {object} context.data - Datos
       * @param {sap.m.DateRangeSelection} context.field - Campo
       * @returns {void} - Noting to return.
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      setDateRangeSelectionData({ data, field }) {
        field
          .setDateValue(new Date(moment(data.LOW)))
          .setSecondDateValue(new Date(moment(data.HIGH)))
      },
      /**
       * @function
       * @name cleanDateRangeSelection
       * @description - Limpiar control
       *
       * @private
       * @param {object} context
       * @param {sap.m.DateRangeSelection} context.field - Campo
       * @returns {void} - Noting to return.
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      cleanDateRangeSelection({ field }) {
        field.setValue(undefined)
      },
    }
  }
)
