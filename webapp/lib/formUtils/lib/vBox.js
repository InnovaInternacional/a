/*!
 * ${copyright}
 */
sap.ui.define(['com/innova/sige/model/variant/ItemVariant'], (ItemVariant) => {
  /**
   * @function
   * @name isSelectedAll
   * @description - Seleccionado todo
   *
   * @private
   * @param {object} checkboxGroup - Grupo de CheckBox
   * @returns {boolean}
   *
   * @author Edwin Valencia <evalencia@innovainternacional.biz>
   * @version ${version}
   */
  const isSelectedAll = (checkboxGroup) =>
    checkboxGroup.every((checkbox) => checkbox.getSelected())

  /**
   * @function
   * @name buildData
   * @description - Obtener datos del grupo de CheckBox
   *
   * @private
   * @param {object} context
   * @param {object[]} context.checkboxGroup - Grupo de controles CheckBox
   * @param {string} context.name - Nombre del campo
   * @returns {object}
   *
   * @author Edwin Valencia <evalencia@innovainternacional.biz>
   * @version ${version}
   */
  const buildData = ({ checkboxGroup, name }) => {
    const req = {}
    const selectedAll = isSelectedAll(checkboxGroup)
    if (selectedAll) {
      const [checkbox] = checkboxGroup
      req[`${name}`] = checkbox.data('key')
    } else {
      const [, ...checkboxs] = checkboxGroup
      checkboxs.forEach((checkbox) => {
        const selected = checkbox.getSelected()
        if (selected) {
          req[`${name}`] ??= ''
          req[`${name}`] = req[`${name}`] + checkbox.data('key')
        }
      })
    }
    return req
  }

  return {
    /**
     * @function
     * @name getVBoxData
     * @description - Obtener datos del control VBox
     *
     * @private
     * @param {object} context
     * @param {boolean} context.isVariant - Es variante
     * @param {object} context.field - Control VBox
     * @param {string} context.functionName - Nombre de la función para el backend
     * @param {string} context.group - Grupo al que pertenece el campo
     * @returns {object}
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    getVBoxData({ isVariant, field, functionName, group }) {
      const type = field.data('type')
      const name = field.data('name')
      let req = {}
      if (type === 'CheckBoxGroup') {
        const checkboxGroup = field.getItems()
        req = buildData({ checkboxGroup, name })

        if (isVariant) {
          return {
            [`${name}`]: [
              new ItemVariant({
                fieldname: name,
                function: functionName,
                group,
                low: req[`${name}`],
                tabname: field.data('tabname'),
              }),
            ],
          }
        }
      }
      return req
    },
    /**
     * @function
     * @name setVBoxData
     * @description - Obtener datos del control VBox
     *
     * @private
     * @param {object} context
     * @param {object} context.data - Datos
     * @param {sap.m.VBox} context.field - Campo
     * @returns {void} - Noting to return.
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    setVBoxData({ data, field }) {
      const checkboxGroup = field.getItems()
      checkboxGroup.forEach((/** @type {sap.m.CheckBox} */ checkbox) => {
        const key = checkbox.data('key')
        if (data.LOW.includes(key)) {
          const path = checkbox.getBindingPath('selected')
          const binding = checkbox.getBinding('selected')
          const model = /** @type {sap.ui.model.json.JSONModel} */ (
            binding.getModel()
          )

          if (model) {
            model.setProperty(path, true)
          } else {
            checkbox.fireSelect({ selected: true })
          }
        }
      })
    },
    /**
     * @function
     * @name cleanVBox
     * @description - Limpiar control
     *
     * @private
     * @param {object} context
     * @param {sap.m.VBox} context.field - Campo
     * @returns {void} - Noting to return.
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    cleanVBox({ field }) {
      const type = field.data('type')
      if (type === 'CheckBoxGroup') {
        const checkboxGroup = field.getItems()
        checkboxGroup.forEach((/** @type {sap.m.CheckBox} */ checkbox) => {
          const path = checkbox.getBindingPath('selected')
          const binding = checkbox.getBinding('selected')
          const model = /** @type {sap.ui.model.json.JSONModel} */ (
            binding.getModel()
          )

          if (model) {
            model.setProperty(path, false)
          } else {
            checkbox.fireSelect({ selected: false })
          }
        })
      }
    },
  }
})
