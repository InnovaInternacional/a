/*!
 * ${copyright}
 */
sap.ui.define([], () => ({
  /**
   * @function
   * @name getComboBoxData
   * @description - Obtiene el valor del control ComboBox
   *
   * @public
   * @param {object} context
   * @param {sap.m.ComboBox} context.field - Control
   * @returns {object}
   *
   * @author Edwin Valencia <evalencia@innovainternacional.biz>
   * @version ${version}
   */
  getComboBoxData({ field }) {
    return { [field.getName()]: field.getSelectedKey() }
  },

  /**
   * @function
   * @name setComboBoxData
   * @description - Set comboBox data.
   *
   * @public
   * @param {object} context
   * @param {sap.m.ComboBox} context.field - Campo
   * @param {string|object} context.data - Datos
   * @returns {void} - Noting to return.
   *
   * @author Edwin Valencia <evalencia@innovainternacional.biz>
   * @version ${version}
   */
  setComboBoxData({ field, data }) {
    field.setSelectedKey(data[field.getName()])
  },
  /**
   * @function
   * @name isComboBoxValid
   * @description - Is ComboBox valid.
   *
   * @public
   * @param {object} context
   * @param {sap.m.ComboBox} context.field - field
   * @returns {boolean}
   *
   * @author Edwin Valencia <evalencia@innovainternacional.biz>
   * @version ${version}
   */
  isComboBoxValid({ field }) {
    return !!field.getSelectedKey()
  },
  /**
   * @function
   * @name cleanMultiInput
   * @description - Limpiar control
   *
   * @public
   * @param {object} context
   * @param {sap.m.ComboBox} context.field - Campo
   * @returns {void} - Noting to return.
   *
   * @author Edwin Valencia <evalencia@innovainternacional.biz>
   * @version ${version}
   */
  cleanComboBox({ field }) {
    field.resetProperty('selectedKey')
  },
}))
