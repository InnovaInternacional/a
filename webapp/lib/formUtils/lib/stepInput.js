/*!
 * ${copyright}
 */
sap.ui.define([], () =>
  /**
   * @function
   * @name getStepInputData
   * @description - Obtiene el valor del control StepInput
   *
   * @public
   * @param {object} context
   * @param {sap.m.StepInput} context.field - Control
   * @returns {object}
   *
   * @author Edwin Valencia <evalencia@innovainternacional.biz>
   * @version ${version}
   */
  ({
    getStepInputData({ field }) {
      return { [field.getName()]: field.getValue() }
    },
  })
)
