/*!
 * ${copyright}
 */
sap.ui.define(['com/innova/sige/model/variant/ItemVariant'], (ItemVariant) => ({
  /**
   * @function
   * @name getSwitchData
   * @description - Obtiene el valor del control Switch
   *
   * @public
   * @param {object} context
   * @param {boolean} context.isVariant - Es variante
   * @param {sap.m.Switch} context.field - Control VBox
   * @param {string} context.functionName - Nombre de la función para el backend
   * @param {string} context.group - Grupo al que pertenece el campo
   * @returns {object}
   *
   * @author Edwin Valencia <evalencia@innovainternacional.biz>
   * @version ${version}
   */
  getSwitchData({ isVariant, field, functionName, group }) {
    const name = field.getName()
    const state = field.getState() ? 'X' : null
    if (isVariant) {
      return {
        [name]: new ItemVariant({
          fieldname: name,
          function: functionName,
          group,
          low: state,
          tabname: field.data('tabname'),
        }),
      }
    }
    return { [name]: state }
  },
  /**
   * @function
   * @name setSwitchData
   * @description - Obtener datos del control
   *
   * @private
   * @param {object} context
   * @param {sap.m.Switch} context.field - Campo
   * @param {object} context.data - Datos
   * @returns {void} - Noting to return.
   *
   * @author Edwin Valencia <evalencia@innovainternacional.biz>
   * @version ${version}
   */
  setSwitchData({ field, data }) {
    const name = field.getName()
    const value = data[name]
    const state = value === 'X'
    field.setState(state)
    field.fireChange({ state })
  },
  /**
   * @function
   * @name cleanSwitch
   * @description - Limpiar control
   *
   * @private
   * @param {object} context
   * @param {sap.m.Switch} context.field - Campo
   * @returns {void} - Noting to return.
   *
   * @author Edwin Valencia <evalencia@innovainternacional.biz>
   * @version ${version}
   */
  cleanSwitch({ field }) {
    field.setState()
    field.fireChange({ state: false })
  },
}))
