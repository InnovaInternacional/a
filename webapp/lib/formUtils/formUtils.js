/*!
 * ${copyright}
 */
sap.ui.define(
  [
    './lib/comboBox',
    './lib/dateRangeSelection',
    './lib/datePicker',
    './lib/input',
    './lib/multiComboBox',
    './lib/multiInput',
    './lib/stepInput',
    './lib/switch',
    './lib/vBox',
    'com/innova/sige/utils/isFloatNumber',
  ],
  (
    comboBox,
    dateRangeSelection,
    datePicker,
    input,
    multiComboBox,
    multiInput,
    stepInput,
    vSwitch,
    vBox,
    isFloatNumber
  ) => {
    const formUtils = {
      ...comboBox,
      ...dateRangeSelection,
      ...datePicker,
      ...input,
      ...multiComboBox,
      ...multiInput,
      ...stepInput,
      ...vSwitch,
      ...vBox,

      /* =========================================================== */
      /* begin: handler methods                                      */
      /* =========================================================== */

      /**
       * @function
       * @name onLiveChangeTypeNumberP
       * @description - Valida los campos de tipo P.
       *
       * @public
       * @param {sap.ui.base.Event} oEvent An Event object consisting of an id, a source and a map of parameters
       * @returns {void} - No retona nada.
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      onLiveChangeTypeNumberP(oEvent) {
        const oInput = /** @type {sap.m.Input} */ (oEvent.getSource())
        const sValue = oEvent.getParameter('value')
        const bValidate = isFloatNumber(sValue)

        if (!bValidate) {
          oInput.setValue('').fireChange({ value: '' })
        }
      },

      /* =========================================================== */
      /* finish: handler methods                                     */
      /* =========================================================== */

      /* =========================================================== */
      /* begin: utils methods                                        */
      /* =========================================================== */

      /**
       * @function
       * @name validateForm
       * @description - Obtener datos de los campos
       *
       * @public
       * @param {object} context
       * @param {object[]} context.formContainers - FormContainers del formulario
       * @returns {boolean}
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      validateForm({ formContainers }) {
        let bValid = true

        formContainers
          .map(formUtils._mapFields)
          .filter(formUtils._isRequiredField)
          .forEach((field) => {
            const name = formUtils._getNameControl(field)
            const strategy = `is${name}Valid`
            const isValid = formUtils[strategy]({ field })

            if (!isValid) {
              field.setValueState('Error')
              bValid = false
            } else {
              field.setValueState('None')
            }
          })

        return bValid
      },
      /**
       * @function
       * @name getDataFromFields
       * @description - Obtener datos de los campos
       *
       * @public
       * @param {object} context
       * @param {object[]} context.formElements - FormElements del contenedor de formulario
       * @param {string} context.functionName - Nombre de la función para el backend
       * @param {string} context.group - Grupo al que pertenece el campo
       * @param {boolean} context.isVariant - Es variante
       * @returns {object}
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      getDataFromFields({ formElements, functionName, group, isVariant }) {
        return formElements.reduce((accumulator, element) => {
          let acc = JSON.parse(JSON.stringify(accumulator))
          const [field] = element.getFields()
          const name = formUtils._getNameControl(field)
          const strategy = `get${name}Data`
          if (formUtils[`${strategy}`]) {
            acc = {
              ...acc,
              ...formUtils[`${strategy}`]({
                field,
                functionName,
                group,
                isVariant,
              }),
            }
          }
          return acc
        }, {})
      },
      /**
       * @function
       * @name getFormData
       * @description - Get form data.
       *
       * @public
       * @param {sap.ui.layout.form.Form} form - Form to get data
       * @returns {object}
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      getFormData(form) {
        return form.getFormContainers().reduce((acc, container) => {
          const result = formUtils.getDataFromFields({
            formElements: container.getFormElements(),
          })
          return { ...acc, ...result }
        }, {})
      },
      /**
       * @function
       * @name getDataFromFormContainer
       * @description - Get data from form container.
       *
       * @public
       * @param {sap.ui.layout.form.FormContainer} formContainer - FormContainer to get data
       * @returns {object}
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      getDataFromFormContainer(formContainer) {
        return formUtils.getDataFromFields({
          formElements: formContainer.getFormElements(),
        })
      },
      /**
       * @function
       * @name setDataInFields
       * @description - Establecer los datos de los campos
       *
       * @public
       * @param {object} context
       * @param {object[]} context.formElements - FormElements del contenedor de formulario
       * @param {object} context.data - Datos
       * @returns {void} - No retorna nada
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      setDataInFields({ formElements, data }) {
        formElements.forEach((element) => {
          const [field] = element.getFields()
          const name = formUtils._getNameField(field)
          if (data[`${name}`] || name === data.FIELDNAME) {
            const nameField = formUtils._getNameControl(field)
            const strategy = `set${nameField}Data`
            if (formUtils[`${strategy}`]) {
              formUtils[`${strategy}`]({ data, field })
            }
          }
        })
      },
      /**
       * @function
       * @name cleanFields
       * @description - Limpiar campos
       *
       * @public
       * @param {object} context
       * @param {object[]} context.formElements - FormElements del contenedor de formulario
       * @returns {void} - No retorna nada
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      cleanFields({ formElements }) {
        formElements.forEach((element) => {
          const [field] = element.getFields()
          const className = field.getMetadata().getElementName()
          const name = className.split('.').pop()
          const strategy = `clean${name}`
          if (formUtils[`${strategy}`]) {
            formUtils[`${strategy}`]({
              field,
            })
          }
        })
      },

      /* =========================================================== */
      /* begin: internal methods                                     *
      /* =========================================================== */

      /**
       * @function
       * @name _getNameField
       * @description - Obtener nombre del campo
       *
       * @private
       * @param {object} field - Campo
       * @returns {string}
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      _getNameField(field) {
        return (field.getName && field.getName()) || field.data('name')
      },
      /**
       * @function
       * @name _isRequiredField
       * @description - Is required field
       *
       * @private
       * @param {object} field - field
       * @returns {boolean}
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      _isRequiredField(field) {
        return field.getRequired && field.getRequired()
      },
      /**
       * @function
       * @name _mapFields
       * @description - Mapear campos
       *
       * @private
       * @param {sap.ui.layout.form.FormContainer} container - Container
       * @returns {object}
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      _mapFields(container) {
        const [element] = container.getFormElements()
        const [field] = element.getFields()
        return field
      },
      /**
       * @function
       * @name _getNameControl
       * @description - Obtener nombre del Control
       *
       * @private
       * @param {object} control - Control
       * @returns {string}
       *
       * @author Edwin Valencia <evalencia@innovainternacional.biz>
       * @version ${version}
       */
      _getNameControl(control) {
        const className = control.getMetadata().getElementName()
        return className.split('.').pop()
      },

      /* =========================================================== */
      /* finish: internal methods                                    *
      /* =========================================================== */
    }
    return formUtils
  }
)
