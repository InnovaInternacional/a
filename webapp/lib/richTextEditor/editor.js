/*!
 * ${copyright}
 */
sap.ui.define([], () => ({
  /**
   * @function
   * @name initializeQuillEditor
   * @description - Se ejecuta después de que se renderizo la vista.
   *
   * @public
   * @param {object} context - Vista que se esta renderizando.
   * @param {HTMLDivElement} context.container - Vista que se esta renderizando.
   * @param {string} context.placeholder - Vista que se esta renderizando.
   * @returns {object}
   *
   * @author Edwin Valencia <evalencia@innovainternacional.biz>
   * @version ${version}
   */
  // @ts-ignore
  initializeQuillEditor({ container, placeholder, setup }) {
    // @ts-ignore
    return new tinymce.Editor(
      `${container.id}`,
      {
        plugins: [
          'advlist autolink lists link image charmap print preview anchor',
          'searchreplace visualblocks code fullscreen',
          'insertdatetime media table paste code help wordcount',
        ],
        toolbar:
          'undo redo | formatselect | ' +
          'bold italic backcolor | alignleft aligncenter ' +
          'alignright alignjustify | bullist numlist outdent indent | ' +
          'removeformat | help',
        content_style:
          'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }',
        placeholder,
        setup,
      },
      // @ts-ignore
      tinymce.EditorManager
    )
  },
  /**
   * @function
   * @name removeEditorManager
   * @description - Remove all tinymce instances.
   *
   * @public
   * @returns {void} Noting to return.
   *
   * @author Edwin Valencia <evalencia@innovainternacional.biz>
   * @version ${version}
   */
  removeEditorManager() {
    // @ts-ignore
    tinymce.EditorManager.remove()
  },
}))
