/*!
 *  ${copyright}
 */
sap.ui.define(
  [
    'sap/base/Log',
    'sap/m/MessageBox',
    './service/http',
    './utils/showToast',
    './model/constant',
  ],
  (Log, MessageBox, http, showToast, constant) => {
    const showMessageBox = ({ description, error, statusCode }) => {
      MessageBox.show(description, {
        icon: MessageBox.Icon.ERROR,
        title: `${statusCode} | ${error}`,
        actions: [MessageBox.Action.CLOSE],
        styleClass:
          'sapUiResponsivePadding--header sapUiResponsivePadding--content sapUiResponsivePadding--footer',
        contentWidth: '100px',
      })
    }

    const ERROR_HANDLERS = {
      500: showMessageBox,
      400: showMessageBox,
      401: (_, controller) => controller.singOut(),
      404: () => showToast(this.getTextPoolMessage('K142')),
      defaultError: (error, controller) => {
        // Something happened in setting up the request that triggered an Error
        showToast(error.message || error.toString(), {
          duration: 3000,
          width: 'auto',
          closeOnBrowserNavigation: false,
        })
        Log.error(
          error.toString(),
          error.stack,
          controller.getView().getControllerName()
        )
      },
    }

    const axiosError = (error, controller) => {
      if (error.response) {
        const { status, data } = error.response
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx

        Log.error(error, status, data.statusText)

        if (data.description) {
          if (data.description.code === 'TOKEN_EXPIRED') {
            MessageBox.warning(
              'Su token ha expirado, ¿Desea recibir otro link de registro?',
              {
                actions: [MessageBox.Action.OK, MessageBox.Action.CANCEL],
                emphasizedAction: MessageBox.Action.OK,
                onClose: (sAction) => {
                  if (sAction === 'OK') {
                    Promise.resolve(
                      http.post(`${constant.api.REFRESH_TOKEN_PATH}`, {
                        token: this._oQuery.state,
                      })
                    )
                      .then(() => {
                        showToast(this.getResourceBundle().getText('0081'))
                        this.getRouter().navTo('login')
                      })
                      .catch(this.errorHandler.bind(this))
                  } else {
                    this.getRouter().navTo('login')
                  }
                },
              }
            )
          } else {
            showMessageBox({
              description: data.description.message,
              error: data.error,
              statusCode: data.statusCode,
            })
            return
          }
        }
        const handler =
          ERROR_HANDLERS[`${error.statusCode}`] || ERROR_HANDLERS.defaultError
        handler(error, controller)
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js (error.request)
        showToast(`${error.name}: ${error.message}`)
        Log.error(error.name, error.message)
      }
    }

    return (error, controller) => {
      if (error.isAxiosError) {
        axiosError(error, controller)
      } else if (http.isCancel(error)) {
        Log.info(error.name, error.message)
      } else {
        ERROR_HANDLERS.defaultError(error, controller)
      }
    }
  }
)
