/*!
 * ${copyright}
 */
sap.ui.define(
  ['sap/ui/model/resource/ResourceModel'],
  (ResourceModel) => {
    const oResourceBundle = new ResourceModel({
      bundleName: 'com.innova.sige.i18n.i18n',
    }).getResourceBundle()

    /**
     * Entrada proveedor of Evaluation Criteria.
     *
     * @enum {object}
     * @public
     * @alias com.innova.sige.model.process.EntProvEvaluationCriteria
     */

    return {
      /**
       * No se necesita un valor de referencia
       * @public
       */
      vendors: {
        key: 'vendor',
        text: oResourceBundle.getText('BiddingProcess.0069'),
      },
    }
  },
  /* bExport= */ true
)
