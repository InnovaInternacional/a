/*!
 * ${copyright}
 */
sap.ui.define(
  ['sap/ui/model/json/JSONModel', 'sap/ui/util/Storage', '../numberFormat'],
  (JSONModel, Storage, numberFormat) =>
    JSONModel.extend('com.innova.sipre.model.SessionStorageModel', {
      _STORAGE_KEY: 'oMQDlug5EfvVaKq2JiZ',
      _storage: null,

      /**
       * Fetches the favorites from local storage and sets up the JSON model
       * By default the string "SESSIONSTORAGE_MODEL" is used but it is recommended to set a custom key
       * to avoid name clashes with other apps or other instances of this model class

       * @param {string} sStorageKey storage key that will be used as an id for the local storage data
       * @param {Object} oSettings settings objec that is passed to the JSON model constructor
       * @return {sap.ui.demo.cart.model.LocalStorageModel} the local storage model instance
       */
      constructor: function constructor(...args) {
        const [sStorageKey] = args
        // call super constructor with everything from the second argument
        JSONModel.apply(this, [].slice.call(args, 1))
        this.setSizeLimit(1000000)

        // override default storage key
        if (sStorageKey) {
          this._STORAGE_KEY = sStorageKey
        }
        this._storage = new Storage(Storage.Type.session, this._STORAGE_KEY)
        // load data from local storage
        this._loadData()

        return this
      },

      /**
       * Loads the current state of the model from local storage
       */
      _loadData: function _loadData() {
        const sJSON = this._storage.get(this._STORAGE_KEY)

        if (sJSON) {
          const oData = JSON.parse(sJSON)
          const { sepmil } = oData.sysParams
          const { sepdec } = oData.sysParams

          numberFormat.getNumberFormat(sepmil, sepdec)
          numberFormat.getFormatOptions(sepmil, sepdec)
          this.setData(oData)
        }
        this._bDataLoaded = true
      },

      /**
       * Saves the current state of the model to local storage
       */
      _storeData: function _storeData() {
        const oData = this.getData()

        // update local storage with current data
        const sJSON = JSON.stringify(oData)
        this._storage.put(this._STORAGE_KEY, sJSON)
      },

      /**
       * Sets a property for the JSON model
       * @override
       */
      setProperty: function setProperty(...args) {
        JSONModel.prototype.setProperty.apply(this, args)
        this._storeData()
      },

      /**
       * Sets the data for the JSON model
       * @override
       */
      setData: function setData(...args) {
        JSONModel.prototype.setData.apply(this, args)
        // called from constructor: only store data after first load
        if (this._bDataLoaded) {
          this._storeData()
        }
      },

      /**
       * Refreshes the model with the current data
       * @override
       */
      refresh: function refresh(...args) {
        JSONModel.prototype.refresh.apply(this, args)
        this._storeData()
      },

      /**
       * Destroy the model with the current data
       * @override
       */
      destroy: function destroy(...args) {
        JSONModel.prototype.destroy.apply(this, args)
        this._bDataLoaded = false
        this._storage.remove(this._STORAGE_KEY)
      },
    })
)
