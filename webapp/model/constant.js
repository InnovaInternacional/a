/*!
 * ${copyright}
 */
// Provides enumeration com.innova.sige.model.Constant
sap.ui.define(
  /**
   * Constantes.
   *
   * @enum {string}
   * @public
   * @alias com.innova.sige.model.Constant
   */
  () => ({
    /**
     * Datos de sesión.
     * @public
     */
    DEFAULT_SESSION: {
      token: '8085940a1c5a75bc2ed387fb5bcb9d92',
      url: '__cfduid',
    },

    /**
     * Endpoints API
     * @public
     */
    api: {
      VENDOR_LOGIN: 'auth/login',
      VENDOR_REGISTER: 'auth/register',
      VENDOR_RESET: 'auth/reset-password',
      OFFERS_PATH: 'offers',
      OFFERS_BY_VENDOR_PATH: 'offers/vendor',
      PROCESS_CATEGORY_PATH: 'process-category',
      VENDOR_PATH: 'vendor',
      ATTACHMENTS_PATH: 'attachments',
      PROCESS_PATH: 'process',
      POSITION_CRITERIA: 'position-criteria',
      VENDOR_ALL_PATH: 'vendors/all',
      VENDOR_INVITATION_PATH: 'mail/registration',
      VENDORS_PATH: 'vendor',
      SEND_EMAIL_RESET_PATH: 'mail/reset',
      GET_TYPEID_HELP: 'tax-id-types-descriptions',
      GET_COUNTRIES_HELP: 'countries-descriptions',
      GET_REGIONS_HELP: 'region',
      REFRESH_TOKEN_PATH: 'mail/token-refresh',
      PLANT: 'plant',
      STORAGELOCATION: 'storage-location/description',
      PURCHASE: 'purchase/query',
      UNLOAD: 'unload',
      UNITMEASURE: 'unit-measure',
      DELIVERY: 'delivery',
      MATERIAL: 'purchase/material',
      DELIVERY_SEARCH: 'delivery/search',
      DELIVERY_STATUS: 'delivery/status',
      DELIVERY_UPDATE: 'delivery/update',
    },
  }),
  /* bExport= */ true
)
