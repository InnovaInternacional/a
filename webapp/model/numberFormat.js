/*!
 * ${copyright}
 */
sap.ui.define(['sap/ui/core/format/NumberFormat'], (NumberFormat) => {
  let oNumberFormat = null
  let oFormatOptions = null

  const numberFormat = {
    /**
     * @function
     * @name getNumberFormat
     * @description - Obtener una instancia de NumberFormat
     *
     * @public
     * @param {string} sepmil - Separador de miles
     * @param {string} sepdec - Separador decimal
     * @returns {NumberFormat} - Cadena formateada.
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    getNumberFormat(groupingSeparator, decimalSeparator) {
      if (!oNumberFormat) {
        oNumberFormat = NumberFormat.getFloatInstance({
          showMeasure: false,
          useSymbol: true,
          minFractionDigits: 1,
          decimals: 2,
          groupingSeparator,
          decimalSeparator,
        })
      }
      return oNumberFormat
    },
    /**
     * @function
     * @name getFormatOptions
     * @description - Obtiene un objeto con las opciones de formator de los números de SAP.
     *
     * @public
     * @returns {object} - Objeto de formato de opciones.
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    getFormatOptions(...oArg) {
      if (!oFormatOptions) {
        oFormatOptions = {
          groupingSeparator: oArg[0],
          decimalSeparator: oArg[1],
        }
      }
      return oFormatOptions
    },
    /**
     * @function
     * @name getFloatFormat
     * @description - Obtener una instancia Float
     *
     * @public
     * @param {object} context
     * @param {number} context.decimals - Cantidad de decimales
     * @param {number} context.maxIntegerDigits - Cantidad de decimales
     * @returns {NumberFormat} - Instancia
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    getFloatFormat({ decimals = 2, maxIntegerDigits }) {
      const { groupingSeparator, decimalSeparator } =
        numberFormat.getFormatOptions()

      return NumberFormat.getFloatInstance({
        decimalSeparator,
        emptyString: null,
        groupingSeparator,
        maxFractionDigits: decimals,
        maxIntegerDigits,
        showMeasure: false,
        useSymbol: true,
      })
    },
    /**
     * @function
     * @name quote
     * @description - Retorna el valor especifico que se desea reemplazar.
     *
     * @public
     * @param {string} sRegex - Expresión regular.
     * @returns {string} - string.
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    quote(sRegex) {
      return sRegex.replace(/([.?*+^$[\]\\(){}|-])/g, '\\$1')
    },
    /**
     * @function
     * @name getRegExtFloat
     * @description - Retorna la expresión regular para válidar si un string formateado es un número con formato válido.
     *
     * @public
     * @returns {string} - Expresión regular.
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    getRegExtFloat() {
      const sGroupingSeparator = numberFormat.quote(
        oFormatOptions.groupingSeparator
      )
      const sDecimalSeparator = numberFormat.quote(
        oFormatOptions.decimalSeparator
      )

      return `^-?(?:\\d+|\\d{1,3}(?:[\\s${sGroupingSeparator}]\\d{3})+)(?:[${sDecimalSeparator}]\\d+)?$`
    },
  }
  return numberFormat
})
