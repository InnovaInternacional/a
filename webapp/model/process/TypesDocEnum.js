/*!
 * ${copyright}
 */
sap.ui.define(
  () =>
    /**
     * Types of Document.
     *
     * @enum {string}
     * @public
     * @alias com.innova.sige.model.process.TypesDocEnum
     */

    ({
      /**
       * Consultar layout disponibles
       * @public
       */
      PRESELECCION: 1,

      /**
       * Obtener el detalle de un layout
       * @public
       */
      LIC_COT: 2,
    }),
  /* bExport= */ true
)
