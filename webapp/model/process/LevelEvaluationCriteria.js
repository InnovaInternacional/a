/*!
 * ${copyright}
 */
sap.ui.define(
  ['sap/ui/model/resource/ResourceModel'],
  (ResourceModel) => {
    const oResourceBundle = new ResourceModel({
      bundleName: 'com.innova.sige.i18n.i18n',
    }).getResourceBundle()

    /**
     * Level of Evaluation Criteria.
     *
     * @enum {object}
     * @public
     * @alias com.innova.sige.model.process.LevelEvaluationCriteria
     */

    return {
      /**
       * Tipo de evaluación de criterios COMERCIAL
       * @public
       */
      C: {
        key: 'C',
        text: oResourceBundle.getText('0069'),
      },

      /**
       * Tipo de evaluación de criterios TECNICO
       * @public
       */
      P: {
        key: 'P',
        text: oResourceBundle.getText('0070'),
      },
    }
  },
  /* bExport= */ true
)
