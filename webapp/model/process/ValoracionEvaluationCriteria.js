/*!
 * ${copyright}
 */
sap.ui.define(
  ['sap/ui/model/resource/ResourceModel'],
  (ResourceModel) => {
    const oResourceBundle = new ResourceModel({
      bundleName: 'com.innova.sige.i18n.i18n',
    }).getResourceBundle()

    /**
     * Valoracion of Evaluation Criteria.
     *
     * @enum {object}
     * @public
     * @alias com.innova.sige.model.process.ValoracionEvaluationCriteria
     */

    return {
      /**
       * Valoracion clasificacion manual
       * @public
       */
      X: {
        key: 'X',
        text: oResourceBundle.getText('0071'),
      },
      /**
       * Valoracion Automatico - menor es mejor
       * @public
       */
      Y: {
        key: 'Y',
        text: oResourceBundle.getText('0072'),
      },
      /**
       * Valoracion Automatico - mayor es mejor
       * @public
       */
      Z: {
        key: 'Z',
        text: oResourceBundle.getText('0073'),
      },
    }
  },
  /* bExport= */ true
)
