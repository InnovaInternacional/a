/*!
 * ${copyright}
 */
sap.ui.define(
  () =>
    /**
     * Types of Evaluation Criteria.
     *
     * @enum {string}
     * @public
     * @alias com.innova.sige.model.process.TypesEvaluationCriteria
     */

    ({
      /**
       * Tipo de evaluación de criterios COMERCIAL
       * @public
       */
      C: 'C',

      /**
       * Tipo de evaluación de criterios TECNICO
       * @public
       */
      T: 'T',
    }),
  /* bExport= */ true
)
