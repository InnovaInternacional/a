/*!
 * ${copyright}
 */
sap.ui.define(
  ['sap/ui/model/resource/ResourceModel'],
  (ResourceModel) => {
    const oResourceBundle = new ResourceModel({
      bundleName: 'com.innova.sige.i18n.i18n',
    }).getResourceBundle()

    /**
     * Entrada proveedor of Evaluation Criteria.
     *
     * @enum {object}
     * @public
     * @alias com.innova.sige.model.process.EntProvEvaluationCriteria
     */

    return {
      /**
       * No se necesita un valor de referencia
       * @public
       */
      A: {
        key: 'A',
        text: oResourceBundle.getText('0074'),
      },
      /**
       * Número
       * @public
       */
      B: {
        key: 'B',
        text: oResourceBundle.getText('0075'),
        prop: 'number',
      },
      /**
       * Si/No
       * @public
       */
      C: {
        key: 'C',
        text: oResourceBundle.getText('0076'),
        prop: 'yesOrNo',
      },
      /**
       * Fecha
       * @public
       */
      D: {
        key: 'D',
        text: oResourceBundle.getText('0077'),
        prop: 'date',
      },
      /**
       * Texto
       * @public
       */
      E: {
        key: 'E',
        text: oResourceBundle.getText('0078'),
        prop: 'text',
      },
    }
  },
  /* bExport= */ true
)
