/*!
 * ${copyright}
 */
sap.ui.define([], () => {
  const WHITE = '#F3F4F6'
  const GRAY = '#A5A5A5'
  const BLUE = '#0C8BF7'
  const BLACK = '#1F2937'
  const RED = '#F74B4D'
  const GREEN = '#61BE19'

  return {
    /**
     * @function
     * @name buildStateOne
     * @description - Construir estado uno
     *
     * @public
     * @param {object} context
     * @param {string} context.icon - Icono
     * @param {string} context.color - Color
     * @returns {object}
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    buildStateOne({ icon, color }) {
      const obj = {
        '00': {
          ICON: 'sap-icon://unlocked',
          COLOR: WHITE,
          CLASS: '',
        },
        11: {
          ICON: 'sap-icon://unlocked',
          COLOR: GRAY,
          CLASS: '',
        },
        32: {
          ICON: 'sap-icon://unlocked',
          COLOR: BLUE,
          CLASS: '',
        },
        43: {
          ICON: 'sap-icon://decline',
          COLOR: BLACK,
          CLASS: '',
        },
      }
      return obj[`${icon}${color}`]
    },
    /**
     * @function
     * @name buildStateTwo
     * @description - Construir estado dos
     *
     * @public
     * @param {object} context
     * @param {string} context.icon - Icono
     * @param {string} context.color - Color
     * @returns {object}
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    buildStateTwo({ icon, color }) {
      const defaultState = {
        ICON: 'sap-icon://cart',
        COLOR: WHITE,
        CLASS: '',
      }
      const obj = {
        10: {
          ICON: 'sap-icon://cart',
          COLOR: GRAY,
          CLASS: '',
        },
        22: {
          ICON: 'sap-icon://cart-5',
          COLOR: BLUE,
          CLASS: '',
        },
        32: {
          ICON: 'sap-icon://cart-full',
          COLOR: BLUE,
          CLASS: '',
        },
      }

      return obj[`${icon}${color}`] || defaultState
    },
    /**
     * @function
     * @name buildStateThree
     * @description - Construir estado tres
     *
     * @public
     * @param {object} context
     * @param {string} context.icon - Icono
     * @param {string} context.color - Color
     * @returns {object}
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    buildStateThree({ icon, color }) {
      const obj = {
        '00': {
          ICON: 'sap-icon://flag',
          COLOR: WHITE,
          CLASS: '',
        },
        11: {
          ICON: 'sap-icon://flag',
          COLOR: GRAY,
          CLASS: '',
        },
        32: {
          ICON: 'sap-icon://flag',
          COLOR: BLUE,
          CLASS: '',
        },
        43: {
          ICON: 'sap-icon://decline',
          COLOR: BLACK,
          CLASS: '',
        },
      }
      return obj[`${icon}${color}`]
    },
    /**
     * @function
     * @name buildStateFour
     * @description - Construir estado cuatro
     *
     * @public
     * @param {object} context
     * @param {string} context.icon - Icono
     * @param {string} context.color - Color
     * @returns {object}
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    buildStateFour({ icon, color }) {
      const obj = {
        '00': {
          ICON: 'sap-icon://innova/41',
          COLOR: WHITE,
          CLASS: '',
        },
        11: {
          ICON: 'sap-icon://innova/41',
          COLOR: GRAY,
          CLASS: 'custom-icon',
        },
        14: {
          ICON: 'sap-icon://innova/41',
          COLOR: RED,
          CLASS: 'custom-icon',
        },
        24: {
          ICON: 'sap-icon://innova/42',
          COLOR: RED,
          CLASS: 'custom-icon',
        },
        25: {
          ICON: 'sap-icon://innova/42',
          COLOR: GREEN,
          CLASS: 'custom-icon',
        },
        34: {
          ICON: 'sap-icon://innova/43',
          COLOR: RED,
          CLASS: 'custom-icon',
        },
        35: {
          ICON: 'sap-icon://innova/43',
          COLOR: GREEN,
          CLASS: 'custom-icon',
        },
        54: {
          ICON: 'sap-icon://innova/45',
          COLOR: RED,
          CLASS: 'custom-icon',
        },
        55: {
          ICON: 'sap-icon://innova/45',
          COLOR: GREEN,
          CLASS: 'custom-icon',
        },
        63: {
          ICON: 'sap-icon://innova/46',
          COLOR: BLACK,
          CLASS: 'custom-icon',
        },
      }
      return obj[`${icon}${color}`]
    },
    /**
     * @function
     * @name buildStateFive
     * @description - Construir estado cinco
     *
     * @public
     * @param {object} context
     * @param {string} context.icon - Icono
     * @param {string} context.color - Color
     * @returns {object}
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    buildStateFive({ icon, color }) {
      const obj = {
        '00': {
          ICON: 'sap-icon://innova/51',
          COLOR: WHITE,
          CLASS: '',
        },
        11: {
          ICON: 'sap-icon://innova/51',
          COLOR: GRAY,
          CLASS: '',
        },
        22: {
          ICON: 'sap-icon://innova/52',
          COLOR: BLUE,
          CLASS: '',
        },
        32: {
          ICON: 'sap-icon://innova/51',
          COLOR: BLUE,
          CLASS: '',
        },
      }
      return obj[`${icon}${color}`]
    },
  }
})
