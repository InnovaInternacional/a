// Provides enumeration com.innova.sige.model.item.OptionType
/*!
 * ${copyright}
 */
sap.ui.define(
  () =>
    /**
     * Types of Low Items.
     *
     * @enum {string}
     * @public
     * @alias com.innova.sige.model.item.OptionType
     */

    ({
      /**
       * Items Equals
       * @public
       */
      EQ: 'EQ',

      /**
       * Items Contains
       * @public
       */
      CP: 'CP',

      /**
       * Items Between
       * @public
       */
      BT: 'BT',

      /**
       * Items Mayor a
       * @public
       */
      GT: 'GT',

      /**
       * Items Mayor o Igual a
       * @public
       */
      GE: 'GE',

      /**
       * Items Menor a
       * @public
       */
      LT: 'LT',

      /**
       * Items Menor o Igual a
       * @public
       */
      LE: 'LE',
    }),
  /* bExport= */ true
)
