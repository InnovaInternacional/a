/*!
 * ${copyright}
 */
/**
 * FormItem
 *
 * @namespace
 * @name com.innova.sige.model.item
 * @public
 */

// Proporciona la implementación del modelo FormItem
sap.ui.define(['sap/ui/base/Object'], (Object) =>
  /**
   * Constructor para un nuevo FormItem.
   *
   * Modelo de un item de formulario que se envia a SAP
   *
   * @param {object} oData - Inf
   *
   * @class
   * @name - FormItem
   * @description - Implementación del modelo de FormItem,
   *  que representa un item del Request para el backend.
   *
   *
   * @author Edwin Valencia <evalencia@innovainternacional.biz>
   * @version ${version}
   * @public
   * @alias com.innova.sige.model.item.FormItem
   */
  Object.extend('com.innova.sige.model.item.FormItem', {
    constructor({ tabname, fieldname, sign = 'I', option = 'EQ', low, high }) {
      if (tabname) {
        this.TABNAME = tabname
      }
      if (fieldname) {
        this.FIELDNAME = fieldname
      }
      this.SIGN = sign
      this.OPTION = option
      this.LOW = low
      this.HIGH = high
    },
    /**
     * @function
     * @name setSign
     * @description - Establecer el atributo Sign.
     *
     * @public
     * @param {string} sSign - texto del atributo Sign.
     * @returns {void} Noting to return.
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    setSign(sSign) {
      this.SIGN = sSign
    },
    /**
     * @function
     * @name setOption
     * @description - Establecer el atributo Option.
     *
     * @public
     * @param {string} sOption - texto del atributo Option.
     * @returns {void} Noting to return.
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    setOption(sOption) {
      this.OPTION = sOption
    },
    /**
     * @function
     * @name setOption
     * @description - Establecer el atributo Low.
     *
     * @public
     * @param {string} sLow - texto del atributo Low.
     * @returns {void} Noting to return.
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    setLow(sLow) {
      this.LOW = sLow
    },
    /**
     * @function
     * @name setHigh
     * @description - Establecer el atributo High.
     *
     * @public
     * @param {string} sHigh - texto del atributo High.
     * @returns {void} Noting to return.
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    setHigh(sHigh) {
      this.HIGH = sHigh
    },
  })
)
