sap.ui.define(
  [
    './session/SessionStorageModel',
    'sap/ui/Device',
    'sap/ui/model/BindingMode',
    'sap/ui/model/json/JSONModel',
  ],
  (
    SessionStorageModel,
    Device,
    BindingMode,
    JSONModel
  ) => ({
    createDeviceModel() {
      const oModel = new JSONModel(Device)
      oModel.setDefaultBindingMode('OneWay')
      return oModel
    },
    createMainModel() {
      // create and set main model
      const oMainModel = new SessionStorageModel('MAIN', {
        catalog: {},
        sysParams: {},
        textPool: {},
      })
      oMainModel.setDefaultBindingMode('OneWay')
      return oMainModel
    },
    createStoreModel() {
      // create and set main model
      const oStoreModel = new JSONModel({ req: {}, res: {} })
      oStoreModel.setDefaultBindingMode(BindingMode.OneWay)
      return oStoreModel
    },
  })
)
