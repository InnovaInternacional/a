sap.ui.define(
  [],
  () =>
    /**
     * @function
     * @name getObjTextProp
     * @description - Get object text
     *
     * @public
     * @param {string} key - Campo ayuda de busqueda
     * @param {object} obj - Campo ayuda de busqueda
     * @returns {string}
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    (key, obj) =>
      obj[`${key}`]?.text
)
