/*!
 * ${copyright}
 */
sap.ui.define(
  ['sap/base/strings/formatMessage'],
  (formatMessage) =>
    /**
     * @function
     * @name formatMessage
     * @description - Formatea un string con formatMessage.
     *
     * @public
     * @param {object} sPatternString - Cadena con el pattern
     * @param {object} oArgs - Argumentos de la función
     * @returns {string} - String formateado.
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    (sPatternString, ...oArgs) =>
      formatMessage(sPatternString, oArgs)
)
