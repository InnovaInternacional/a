sap.ui.define(
  [],
  () =>
    /**
     * @function
     * @name hasSearchHelp
     * @description - Validar si el campo tiene ayuda de busqueda
     *
     * @public
     * @param {string} f4availabl - Campo ayuda de busqueda
     * @returns {boolean}
     *
     * @author Edwin Valencia <evalencia@innovainternacional.biz>
     * @version ${version}
     */
    (f4availabl) =>
      f4availabl === 'X'
)
