sap.ui.define([], () =>
  /**
   * @function
   * @name maxLength
   * @description - Convierte un String a entero, para campos de máxima longitud.
   *
   * @public
   * @param {string} sIntlen - Entero a convertir
   * @returns {number}
   *
   * @author Edwin Valencia <evalencia@innovainternacional.biz>
   * @version ${version}
   */
  (intlen = '5') => {
    const bNotnumber = Number.isNaN(intlen)
    if (bNotnumber) {
      return 0
    }
    return parseInt(intlen, 10)
  }
)
