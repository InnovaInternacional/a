# SiGC - Gestión de compras

Proyecto UI5 para Fiori, aplicación SiGC - Gestión de Compras workbench Front.

### Servicios
* [WSDL]()

* [ODATA](http://innova-s4d.innovainternacional.biz:8001/sap/opu/odata/sap/zsigc_odata_srv/?sap-client=100)

---

## Instalación

```bash
yarn
```

## Uso

```bash
yarn dev
```

___

## Despliegue

- Subir aplicación al Launchpad de Fiori
- Ingresar a la transacción `SE38` dentro de SAP
- Ejecutar el programa `/UI5/UI5_REPOSITORY_LOAD`
  - Nombre de la aplicación SAPUI5 --> `ZSIGC_APP`
  - Hacer click en cargar
  - Seleccionar la carpeta de distribución
- Asignar paquete **ZSIGC**
- Asignar orden de trasporte ej: **S4DK9A00DN**

---

## License

[PRIVATE](https://choosealicense.com/licenses/mit/)
